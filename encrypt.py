#Author: Fernando
#Date: 4/29/2014
#Parameter: password to be encrypted
#Result: ASCII encrypted password
#Description: Using given command line parameter, it process and encryptes it and show ASCII encrypted string in screen. Makes a processing in password before encryption, because password needs to be multiple of 8 (it is required by encrypt API). Characters \ ' " are not repassed by python or debian, so encryptation will not work if parameter contains any of them.

import binascii
import sys
from Crypto.Cipher import DES

print "Note - Don't use characters \ \' \" in parameter you give - encryptation will not work with them"

if len(sys.argv) <> 2:
	print("Usage: encrypt <password>")
elif len(sys.argv[1]) > 30:
	print("Password lenght needs to be less than 30")
else:
	obj=DES.new('abcdefgh', DES.MODE_ECB)
	# prepares password for encryption (concatenate its length with 2 characters, password itself and an optional final string to complete a multiple of 8
	orig_len = '00' + str(len(sys.argv[1]))
	pass_proc = orig_len[-2:] + sys.argv[1]
	if len(pass_proc) % 8 == 0: # if it is already multiple of 8, it is done
	    final_pass = pass_proc
	else: # if it is not multiple of 8, so makes it multiple of 8 concatenating missing characters	
	    pass_proc = pass_proc + 'pcs4kids'    
	    final_pass = pass_proc[:(len(pass_proc) / 8)* 8]
	ciph=obj.encrypt(final_pass)

	print "encrypt = " + binascii.b2a_uu(ciph)  # prints final encryptation result
