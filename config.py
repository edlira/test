#---- Database Credentials -----
dbname="pcs4kids";
dbuser="user";
dbpass="encrypt_pass";

#---- Global Constants -----
NR_TOTAL=1000
LIMIT_NUMBERACCESS = 5 # Max number of records for most accessed items (favorites) in Home Screen
LIMIT_LASTACCESS = 9 # Max number of records for last accessed item (recent) in Home Screen
LIMIT_MINAGE = 0     # Default value of minage when we do not set a special lower bound
LIMIT_MAXAGE = 100   # Default value of maxage when we do not set a special upper bound

INTIAL_DATE = '2001-01-01' # Date 0 (zero) of system. Used when needed handle a null date
RECENT_DAYS = '30' # Maximum number of days with no access for an item be shown when Recent option is chosen in Search feature

HOVER_TIMEOUT = 500 # Time to open menu when user hovers mouse in it

DOCUMENT_PATH='resources/doc/'
VIDEO_PATH='resources/videos/'
GAME_PATH='resources/game/'
AUDIO_PATH='resources/audio/'
IMAGE_PATH='images/'

#---- LOGGING MODE ---------
#True->Debug; False ->Info
DEVELOPER = False
#DEVELOPER = True

#Ordering values
RECENT_ORDER = 0
FAVORITES_ORDER = 1
ALPHA_ORDER = 2

