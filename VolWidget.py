import os

from PyQt4 import QtGui,QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from config import *
import colorsys


class VolWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self)
        self.volumeMeter = 50 #Initial is 50, that is the middle of dial/slider (50% of volume that goes from 0 until 100)
        self.dialSlider = QtGui.QDial(self)
        self.dialSlider.setMaximum(100)
        self.dialSlider.setFixedWidth(40)
        self.dialSlider.setFixedHeight(50)
	self.dialSlider.setObjectName('dialObject')
        # Set background transparent.  Only items drawn in paintEvent
        # will be visible.
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Base, Qt.black)#QtCore.Qt.transparent)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.setPalette(palette)
        self.dialogButton = QtGui.QPushButton(QIcon(IMAGE_PATH+'audio-volume-high.png'),'', self)
        self.dialogButton.setObjectName("volumebutton")

    def get_rgb_from_hue_spectrum(self,percent, start_hue, end_hue):
        # spectrum is red (0.0), orange, yellow, green, blue, indigo, violet (0.9)
        hue = percent * (end_hue - start_hue) + start_hue
        lightness = 0.5
        saturation = 1
        r, g, b = colorsys.hls_to_rgb(hue, lightness, saturation)
        return r * 255, g * 255, b * 255

    #Shows sound volume control slider and mute unmute button and sets functions to handle user actions in them
    def showVolumeControl(self):
        #sound volume dial
        self.dialSlider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.dialSlider.setNotchesVisible(True)
        self.dialSlider.valueChanged[int].connect(self.changeVolume) #function to change volume when dial is moved
        self.dialSlider.setValue(self.volumeMeter)

        #button for opening volume slider
        #volume slider icon
        self.dialogButton.setCheckable(True)
        self.dialogButton.clicked[bool].connect(self.showVolumeDialog) #function to open volume slider

    #Changes volume according where dial/slider indicator is positioned
    def changeVolume(self, value):
        InitialVol=self.volumeMeter
        self.volumeMeter = value # Reevaluate the volume level equal to value change on slider.
        os.system('amixer set Master ' + (str(self.volumeMeter)) + '%') #"amixer" is a debian command to set volume
        self.dialSlider.setValue(self.volumeMeter) # sets dial according slider setting
        self.dialSlider.setObjectName("dialObject") # Setting object name for easy styling with style sheet
        # Preventing the volume from bipping upon initialisation and sounding twice on click
	if self.volumeMeter != InitialVol:
            os.system("aplay sounds/volume.wav")

    def showVolumeDialog(self):
        self.dialog = QtGui.QDialog(self.window, Qt.Tool)
        self.dialog.move(self.volDialogWidth, self.volDialogHeight)
        self.dialog.setWindowTitle("Volume")
        layout = QVBoxLayout(self.dialog)
        self.showVolumeSlider()
        layout.addWidget(self.volSlider, 2, Qt.AlignCenter)
        layout.addWidget(self.muteButton, 1, Qt.AlignCenter)

        self.dialog.resize(90, 150)
        self.dialog.exec_()

    def showVolumeSlider(self):
        #sound volume slider
        self.volSlider = QtGui.QSlider(QtCore.Qt.Vertical, self)
        self.volSlider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.volSlider.valueChanged[int].connect(self.changeVolume) #function to change volume when slider is moved
        #self.volSlider.valueChanged[int].connect(self.playsound)
        self.volSlider.setValue(self.volumeMeter) #Initial comes from dial adjust

        #button for mute, unmute
        self.muteButton = QtGui.QPushButton(QIcon(IMAGE_PATH+'audio-volume-high.png'), '', self) #Initial is unmute
        self.muteButton.setCheckable(True)

        self.muteButton.clicked[bool].connect(self.setMute) #function to mute and unmute when button is clicked

    def setMute(self, pressed):
        if pressed: #when button is pressed, mute is activated
            self.muteButton.setIcon(QIcon(IMAGE_PATH+'audio-volume-high.png')) #change icon for mute
            os.system('amixer set Master mute') #"amixer" is a debian command to set mute
        else:	#when button is pressed again (unpressed), mute is deactivated
            self.muteButton.setIcon(QIcon(IMAGE_PATH+'audio-volume-high.png')) #change icon for volume
            os.system('amixer set Master unmute') #"amixer" is a debian command to set unmute

	##### --------- Start of paint event defination -------------#####
    def paintEvent(self, e):
        posDial = self.dialSlider.pos()
        # print "VolWidget paintEvent - posDial x = " + str(posDial.x())
        w = self.dialSlider.width()
        h = self.dialSlider.height()

        x = (w)/3
        y = (h)/3

        self.dialogButton.setGeometry(posDial.x() + x,posDial.y() + y,w/3,h/3)

        painter = QPainter(self)

        # Determining the base rectangle
        rect = QtCore.QRect(0,0,40,50)

        painter.setRenderHint(painter.Antialiasing)

        gauge_rect = QtCore.QRect(rect)
        size = gauge_rect.size()
        pos = gauge_rect.center()
        gauge_rect.moveCenter( QtCore.QPoint(pos.x()-size.width(), pos.y()-size.height()) )
        gauge_rect.setSize(size*.9)
        gauge_rect.moveCenter(pos)

        refill_rect = QtCore.QRect(gauge_rect)
        size = refill_rect.size()
        pos = refill_rect.center()
        refill_rect.moveCenter( QtCore.QPoint(pos.x()-size.width(), pos.y()-size.height()) )

        # smaller than .9 == thicker gauge
        refill_rect.setSize(size*.9)
        refill_rect.moveCenter(pos)
        painter.setPen(QtCore.Qt.NoPen)
        painter.save()
        grad = QtGui.QConicalGradient(QtCore.QPointF(gauge_rect.center()), 270.0)
        grad.setColorAt(.75, QtCore.Qt.green)
        grad.setColorAt(.5, QtCore.Qt.yellow)
        grad.setColorAt(.1, QtCore.Qt.red)
        painter.setBrush(grad)
        painter.drawPie(gauge_rect, 240*16, self.volumeMeter/100.0 * -300*16)
        painter.restore()

        # painter.setBrush(QtGui.QBrush(dial.scaled(rect.size())))
        painter.drawEllipse(refill_rect)

        super(VolWidget,self).paintEvent(e)

        painter.end()

	##### ----------end of paint event -----------------#####

    def setWindow(self, window):
        self.window = window

    def setVolDialogWidth(self, width):
        self.volDialogWidth = width
        #print 'width',width
        # self.dialSlider.setFixedWidth(self.volDialogWidth)

    def setVolDialogHeight(self, height):
        self.volDialogHeight = height
        #print 'height',height
        # self.dialSlider.setFixedHeight(self.volDialogHeight)
