try:
  import logging
  from PyQt4 import QtGui,QtCore
  from PyQt4.QtCore import *
  from PyQt4.QtGui import *

  # Class for buttons that need to handle hover events.
  class HoverButton(QPushButton):

	mouseHover = pyqtSignal(bool)

	def __init__(self, parent=None):
		QPushButton.__init__(self, parent)
		self.setMouseTracking(True)  # gets mouse move events even when no buttons are held down

	def enterEvent(self, event):
		self.mouseHover.emit(True) # when mouse enters in this button generates a signal with True as parameter

	def leaveEvent(self, event):
		self.mouseHover.emit(False) # when mouse leaves from this button generates a signal with False as parameter
except:
  logging.exception("CM Error Code = HoverButton1")

