#!/usr/bin/python

try:  # puts everything inside a try-except block and logs any exception with its type and its description
  import sys
  import logging
  import subprocess
  import os
  import time
  from PyQt4 import QtGui,QtCore
  from PyQt4.QtCore import *
  from PyQt4.QtGui import *
  from PyQt4.QtCore import Qt
  from PyQt4.QtCore import QSignalMapper, pyqtSignal
  from socket import *
  from query import *
  from PicButton import *
  from HoverButton import *
  from functools import partial
  import Tkinter
  from QxtSpanSlider import QxtSpanSlider
  from VolWidget import *

  #use Tkinter for finding the screen dimensions
  #This is used to open ContentManager in full screen and for calculating dynamic size of the elements in content
  root = Tkinter.Tk()
  screen_width = root.winfo_screenwidth()
  screen_height = root.winfo_screenheight()
  #screen_width=1200
  #screen_height=700

  #load the stylesheet file for design
  styleFile="css/style.stylesheet"
  with open(styleFile,"r") as fh:
        css=fh.read()

  #This class generates all the view and functionalities of ContentManager
  class runContentManager(QWidget):
    def __init__(self, parent=None):
        super(runContentManager,self).__init__(parent)
        #The layout of left menu
        self.menuwidget = QWidget()
        self.menuwidget.setObjectName("menuSection")
        self.menuwidget.setFixedHeight(screen_height)
        self.menuwidget.setFixedWidth(screen_width*0.15)
        #Layout of age ranges in title bar
        self.AGElayout=QHBoxLayout()
	#Slider component for age ranges handling
	self.sliderWidget = QWidget()
        #self.spanSlider = QxtSpanSlider(self.sliderWidget)
        self.spanSlider = QxtSpanSlider()
        #Default value of current Menu in menu bar
        self.currentID="home"
        self.currentname="Home"
	self.setMenuLabel("home")
        #Since the default menu is Home, the default item types displayed in the home screen are items
        self.itype="item"
        #Default values of search option
        self.searchLine=''
        self.searchArray=[]
        self.searchcheckbutton={}
        self.searchrecent=False

	# initializes ordering to be alphabetical
	self.currentorder=ALPHA_ORDER
	comboorder.setCurrentIndex(ALPHA_ORDER) #Alphabetical order

	# initializes selected letter from alphabetical bar to nothing
	self.currentstartletter= ''

	#associate onActivate method with combo item choose
    	comboorder.activated[str].connect(self.combochanged) 

        #Default range of ages: By default we have a very large range in order to not skip any item
        self.minAgeSel=0
        self.maxAgeSel=100
        #Call this method to generate the title of ContentManager
        self.showTitle()
        #Call this method to generate the left Menu
        self.showMenu();
        #Create the popout option for the left menu
        self.showPopoutMenu()
        #Call this method to retrieve from database all the items for the home page screen
        items= gdb.getItems()
        #Show this items in the main view
        self.showContent(items)


    #This method fills the title bar
    def showTitle(self):
        #The layout of title bar
        toplayout = QHBoxLayout(titleBar)
        titleBar.setFixedHeight(screen_height*0.15)
        toplayout.setSpacing(0)

        #show logo
        label=QLabel();
        label.setPixmap(QPixmap(IMAGE_PATH+'logo.jpg'))
        label.setObjectName("logoWidget")
        toplayout.addWidget(label,4)

	#add volume control slider and mute unmute button
	self.volWidget=VolWidget()
	self.volWidget.setWindow(window)

	
        # setting up volume widget and adding to graphics view to circumvent paint event implementation problem
        #self.volWidget.setGeometry(0,0,40,50)

	# set volume slider position
	self.volWidget.setVolDialogWidth(screen_width*0.18)
	self.volWidget.setVolDialogHeight(screen_height*0.1)
	
        scene=QGraphicsScene()#Creating a scene object
    	self.volWidget.showVolumeControl()
    	scene.addWidget(self.volWidget)

    	gview=QGraphicsView(scene)# Creating a graphics view to display volume
	gview.setObjectName('volgraphic')# Naming the volume view for easy styling with stylesheet
	toplayout.addWidget(gview,1)

        #Instantiating the age group class
        age = self.showAgeGroup()
        toplayout.addWidget(self.spanSlider,20)

        #add shutdown button
        shutdownbutton = PicButton(QPixmap(IMAGE_PATH+'shutdown.jpg'))
        shutdownbutton.clicked.connect(self.shutdown);
        toplayout.addWidget(shutdownbutton,1)

    #This method shows a dual slider bar with age ranges in the title bar
    def showAgeGroup(self):
	self.spanSlider.setHandleMovementMode(QxtSpanSlider.NoCrossing)

	#self.spanSlider.setGeometry(QRect(0,0-30,screen_width * 0.7,screen_height * 0.15))
        self.spanSlider.setFixedHeight(screen_height*0.1)
        self.spanSlider.setRange(4, 12)  # sets minimum and maximum values for slider
        self.spanSlider.setSpan(4, 12)
        self.spanSlider.setTickInterval(1)
        self.spanSlider.lowerPositionChanged.connect(self.setMinAgeSel)
        self.spanSlider.upperPositionChanged.connect(self.setMaxAgeSel)

    #This method is triggered by minimum slider selection, gets the age value selected and calls a method to fill screen with data considering this minimum age
    def setMinAgeSel(self, value):
	if value == 4:	# if age selected is 4 then it is the minimum possible, so it is set minimum age parameterized
		value = LIMIT_MINAGE
        self.minAgeSel=value
	self.spanSlider.minAgeSel = self.minAgeSel	# sets for use in spanSlider class
	self.updateContent()

    #This method is triggered by maximum slider selection, gets the age value selected and calls a method to fill screen with data considering this maximum age
    def setMaxAgeSel(self, value):
	if value == 12:	# if age selected is 12 then it is the maximum possible, so it is set maximum age parameterized
		value = LIMIT_MAXAGE
        self.maxAgeSel=value
	self.spanSlider.maxAgeSel = self.maxAgeSel	# sets for use in spanSlider class
	self.updateContent()

    #We filter the current content when an age range is selected
    def updateContent(self):
      try:
        if self.currentID == "search":
	   self.searchItems(self.searchLine, self.searchArray)
        elif self.itype=="item" and self.currentID <>"home":
           self.subcategoryClicked(self.currentID,'',True)
        else:
           self.categoryClicked(self.currentID)
      except:
	logging.exception("CM Error Code = 10")

    #This method shows a a dialog box to promt user if he wants to close the window of ContentManager or shutdown the system
    def shutdown(self):
        quit_msg = "Are you sure you want to shutdown the system?"
        reply = QtGui.QMessageBox.question(self, 'Message', quit_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
           os.system('sudo shutdown -h now')
	# do nothing (command below was enabled when testing, so that Content Manager could be closed without shutdown)
        #else:
        #    self.close()

    #Method to close the window of ContentManager
    def close(self):
      try:
        window.close()
      except:
	    logging.exception("CM Error Code = 5")


    #Create a popout layout to close and open the left menu
    def showPopoutMenu(self):
        self.menupop_out = HoverButton("\n\n".join('>OPEN MENU>') )
        self.menupop_out.setCheckable(True)
        self.menupop_out.setObjectName("popoutMenu")
        menu_width=screen_width*0.02
        menu_height=screen_height
        self.menupop_out.setCheckable(True)
        self.menupop_out.setFixedWidth(menu_width)
        self.menupop_out.setFixedHeight(menu_height)
        menu_layout.addWidget(self.menuwidget)
        menu_layout.addWidget(self.menupop_out)
        #self.setLayout(menu_layout)
        self.menuwidget.hide()

        self.menupop_out.toggled.connect(self.menupressed)
        #self.show()

	# create timer for mouse hover and connect menu pop up button to mouse hover handler and timer to timeout handler
	self.timerhovermenu = QtCore.QTimer(self)
	self.menupop_out.mouseHover.connect(self.hover)
	self.timerhovermenu.timeout.connect(self.hoverTimeout)

    #Implement handler for pression the button which shows or hides the left Menu
    def menupressed(self, e):
      try:
        if not e:
            self.menuwidget.hide()
            self.menupop_out.setText("\n\n".join('>OPEN MENU>'))
        else:
            self.menuwidget.show()
            self.menupop_out.setText("\n\n".join('<CLOSE MENU<'))
	    self.timerhovermenu.stop()   # when user clicks to open menu, stops timer to verify mouse hover in menu
      except:
	logging.exception("CM Error Code = 6")

    # Method to handle mouse hover in popup up menu
    def hover(self, enter):
	if not self.menupop_out.isChecked(): # when menu is already opened, mouse hover is ignored
		self.openmenu = True
		if enter:	# if mouse enters menu, starts a timer until limit time to open menu is reached
			self.timerhovermenu.start(HOVER_TIMEOUT)
		else:		# if mouse leaves menu, stops timer and assigns that menu should not be opened due to mouse hover.
			self.timerhovermenu.stop()
			self.openmenu = False

    # Method to handle reaching mouse hover limit time
    def hoverTimeout(self):
	# when menu is closed and mouse hovered in menu for limit time, opens menu.
	if not self.menupop_out.isChecked() and self.openmenu:
		self.menupop_out.click()
		self.openmenu = False
		self.timerhovermenu.stop()
			
    #This method generates all the buttons of left menu: A button for each Category of items and Search options
    def showMenu(self):
        topmenu_layout = QVBoxLayout()
        bottom_menu_layout=QGridLayout()
        b1=QPushButton(QIcon(IMAGE_PATH+'home.png'),'Home')
	b1.setFlat(False) # it is needed to not be flat because otherwise sometimes buttons remains colored after other are selected (background is not updated for flat buttons)
        b1.setObjectName("leftmenu")
        b1.setIconSize(QSize(30,30))
        signalMapper.setMapping(b1, "home")
        b1.clicked.connect(signalMapper.map)
        categories= gdb.getParentCategories(NR_TOTAL)
	# Creates a table to put buttons inside
	tableWidget = QTableWidget(len(categories) + 1, 1); # creates table with number of main categories retrieved from database plus 1 (Home)
	tableWidget.verticalHeader().setVisible(False)
	tableWidget.verticalHeader().setResizeMode(QHeaderView.Stretch)  # sets table to fill all vertical space
	tableWidget.horizontalHeader().setVisible(False)
	tableWidget.horizontalHeader().setResizeMode(QHeaderView.Stretch) # sets table to fill all horizoncal space
	topmenu_layout.addWidget(tableWidget,1)
	tableWidget.setCellWidget(0, 0, b1);  # inserts button as a widget inside a cell
        el=4
        srow=2
        scol=2
        indexB=0
        for row in categories:
            caticon=IMAGE_PATH
            catid=row[0]
            catname=row[1]
            caticon+=row[2]
            mybutton = QtGui.QPushButton(QIcon(caticon),catname)
	    mybutton.setFlat(False)  # it is needed to not be flat because otherwise sometimes buttons remains colored after other are selected (background is not updated for flat buttons)
            mybutton.setObjectName("leftmenu")
            mybutton.setIconSize(QSize(30,30))
            signalMapper.setMapping(mybutton, catid)
            mybutton.clicked.connect(signalMapper.map)
	    tableWidget.setCellWidget(indexB+1, 0, mybutton);  # inserts button as a widget inside a cell
            menu_item_layout=QHBoxLayout()# Layout to place check buttons and search item buttons
            self.searchcheckbutton[indexB]=QCheckBox()#Check buttons for search item
            self.searchcheckbutton[indexB].setIcon(QIcon(caticon))
            self.searchcheckbutton[indexB].setIconSize(QSize(30,30))
            self.searchcheckbutton[indexB].setChecked(True)
            self.searchArray.append(""+catid)
            self.searchcheckbutton[indexB].clicked.connect(partial(self.filterSearch,self.searchcheckbutton[indexB],catid))
            menu_item_layout.addWidget(self.searchcheckbutton[indexB])
            bottom_menu_layout.addLayout(menu_item_layout,el/srow,el%scol)
            el=el+1
            indexB=indexB+1
        self.searchArrayDefault = list(self.searchArray)
        recenticon=IMAGE_PATH+"recent.gif";
        recent_item_layout=QHBoxLayout()# Layout to place check buttons and search item buttons
        self.searchcheckbutton[indexB]=QCheckBox()#Check buttons for search item
        self.searchcheckbutton[indexB].setIcon(QIcon(recenticon))
        self.searchcheckbutton[indexB].setIconSize(QSize(30,30))
        recent_item_layout.addWidget(self.searchcheckbutton[indexB])# adding checkbox to layout
        bottom_menu_layout.addLayout(recent_item_layout,el/srow,el%scol)# Adding recent search layout to the bottom layout
        self.searchcheckbutton[indexB].clicked.connect(partial(self.recentSearch,self.searchcheckbutton[indexB]))
        signalMapper.mapped[QString].connect(self.categoryClicked)
        searchLabel=QLabel("Search")
        searchLabel.setObjectName("searchLabel")
        searchLabel.setFixedHeight(30)
        bottom_menu_layout.addWidget(searchLabel,0,0,1,2)
        self.qle = QtGui.QLineEdit()
        self.qle.setMaxLength(15)
        self.qle.setPlaceholderText("Search here")
        self.qle.textChanged.connect(partial(self.searchItems))
        bottom_menu_layout.addWidget(self.qle,1,0,1,2)
        topmenu_layout.addLayout(bottom_menu_layout,4)
        emptyW=QWidget()
        emptyW.setFixedHeight(screen_height*0.3)
        #emptyL=QVBoxLayout(emptyW)

        topmenu_layout.addWidget(emptyW,1)
        self.menuwidget.setLayout(topmenu_layout)

    #When a category is selected, this method finds its subcategories
    #If category is "Home" it shows the items
    #In the end this method calls another method showContent() to make this items visible
    @QtCore.pyqtSlot(QString)
    def categoryClicked(self,elID):
      try:
        self.clearAlphabet()
	self.clearSearch()

        self.currentID=elID
        if self.currentID !="home":
           self.currentname=gdb.getNameOfCategoryId(int(self.currentID))
        else:
           self.currentname="Home"
        self.setMenuLabel("category")
        if elID=='home':
            self.itype="item"
            items= gdb.getItems(LIMIT_NUMBERACCESS, LIMIT_LASTACCESS, self.minAgeSel, self.maxAgeSel)
            self.clearContent()
            self.showContent(items)
        else:
            #print elID
            cat_name = gdb.getCategoriesChosen(int(elID),NR_TOTAL,self.minAgeSel,self.maxAgeSel)
            #print cat_name
            self.clearContent()
            self.itype="category"
            self.showContent(cat_name)
      except:
	logging.exception("CM Error Code = 8")
    #@QtCore.pyqtSlot(QString)

    # Sets the label for current menu. It is "Home" when applications starts, the main category correspondent to left menu button clicked or the complete path up to subcategory clicked
    def setMenuLabel(self, ptype):
	#if it is called from subcategory then shows combo ordering
	if ptype == "subcategory" or ptype == "search":
		labelorder.show()
		comboorder.show()
	#if it is not called from subcategory then hides combo ordering
	else:
		labelorder.hide()
		comboorder.hide()
	currentmenu.setText(self.currentname)

    #When a subcategory is selected, the method showContent() is called to show its items and the alphabetical bar is visible
    def subcategoryClicked(self,elID,iname,otherclicked=False):
      try:
        self.currentID=elID
	# if called directly from a click in an item of subcateg screen, positions combo on default and concatenates subcateg with previous category
	if not otherclicked:
		comboorder.setCurrentIndex(ALPHA_ORDER) #Alphabetical order
		self.currentorder=ALPHA_ORDER
		self.currentname= self.currentname + " > " + iname
        self.itype="item"
        self.setMenuLabel("subcategory")
        catlist=[self.currentID]
	# retrieves items from database and stores results for future use in alphabetical choice
        self.items= gdb.getItemsFromCatChosen(catlist,NR_TOTAL,self.minAgeSel,self.maxAgeSel,'',self.currentorder)
        self.clearContent()
        self.showContent(self.items)
	self.clearAlphabet()
	if self.currentorder == ALPHA_ORDER: # Only shows alphabetical choice bar if combo ordering is chosen to be alphabetical
		self.alphabetDisplay()
      except:
	logging.exception("CM Error Code = 9")

    #Find where to place line break(s) in an item name (title) ,if it is longer then given length to minimize the cost(badness)
    #wlist list of the words of the item name
    #startCol starting column (initially it is 0)
    #maxWidth total with
    #return (item_name_transformed, cost)
    def lineBreaks(self, wlist, startCol, maxWidth):
       if len(wlist) == 0:
          return ("", 0)

       word = wlist[0]
       rest = wlist[1:]
       endCol = len(word) + startCol

       #word does not fit on line, signal infinite cost
       if endCol > maxWidth:
          return ("", 1e9) #ininite cost

       #1) NO line break after the word. Rest of item name placed 
       # starting after the given word;it leaves room for the separating space
       r1,c1 = self.lineBreaks(rest, endCol+1, maxWidth)
       r1 = word + " " + r1

       #2) line break after the word. Rest of item name placed 
       # starting on new line
       r2,c2 = self.lineBreaks(rest, 0, maxWidth)
       r2 = word + "\n" + r2

       c2 += (maxWidth - endCol)**3 # compute end of line penalty

       #choose least costly alternative between 1) and 2)
       return (r1,c1) if c1 <= c2 else (r2, c2)

    #This method generates the view of the main content. It shows the items or subcategories.
    def showContent(self,items):
        cnt=0;
        # Column should toggle depending on menu popping -
        col=3 

	# Calculates number of rows of screen as number of items from database divided by number of columns of screen. If division is not integer, means that exists one more incomplete line
	row = (len(items) // col)
	if len(items) % col <> 0:
		row = row + 1

	# Initializes variables related to scroll bar positioning when a letter is chosen from alphabetical choice bar
	positionfound = False # indicates if has already found the position to put scroll bar
	rowposition = 0 # number of row where scroll bar shoud be positioned
	scrollBarItems.setValue(0) # sets scroll bar to beginning

        buttons = {}
        for i in range(row):
            for j in range(col):
                if cnt < len(items):
                    #print cnt 
                    if self.itype == "item":
                       iid=items[cnt][0]
                       iname=str(items[cnt][1])
                       iicon=IMAGE_PATH
                       iicon+=items[cnt][2]
                       ifile=items[cnt][3]
		       if self.currentID == 'home':
                           parentcatname=items[cnt][5]
		       else:
                           parentcatname=items[cnt][4]
                       #self.clearAlphabet()
                       #buttons[(i, j)].setText(iname[0:20])
                       buttons[(i, j)] = QtGui.QToolButton()
                       if(len(iname) <= 20):
                          buttons[(i, j)].setText(iname[0:20])
                       else:
                          buttons[(i, j)].setText(self.lineBreaks(iname.split(" "),0,20)[0])
                       buttons[(i, j)].setToolTip(iname)
                       buttons[(i, j)].setIcon(QIcon(iicon))
                       buttons[(i, j)].setIconSize(QSize(60,60))
                       buttons[(i, j)].setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
                       #buttons[(i, j)].setMinimumHeight(70);
                       buttons[(i, j)].setMinimumWidth(150);
                       display_area.addWidget(buttons[(i, j)], i, j)

		       # if a letter was chosen in alphabetical choice bar AND didn't match item where scroll bar should be positioned then -> see below
		       if self.currentstartletter <> '' and not positionfound:
				rowposition = i   # stores position for scroll bar - if no item matches until end, the last one will be used (scroll bar at end of area)
				# if matched an item with the same letter or with a letter after the one chosen then sets this row for positioning scroll bar (changes control variable to not store position anymore)
				if QString.compare(iname, self.currentstartletter, Qt.CaseInsensitive) >= 0:
					positionfound = True
                       buttons[(i, j)].released.connect(partial(self.itemClicked,iid,ifile,parentcatname))
                    else:
		      try:
                        iid=items[cnt][0]
                        iname=str(items[cnt][1])
                        iicon=IMAGE_PATH+items[cnt][2]
                        buttons[(i, j)] = QtGui.QToolButton()
                        buttons[(i, j)].setText(iname[0:20])
                        buttons[(i, j)].setToolTip(iname)
                        buttons[(i, j)].setIcon(QIcon(iicon))
                        buttons[(i, j)].setIconSize(QSize(30,30))
                        buttons[(i, j)].setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
                        buttons[(i, j)].setMinimumHeight(70);
                        buttons[(i, j)].setMinimumWidth(150);
                        display_area.addWidget(buttons[(i, j)], i, j)
                        buttons[(i, j)].released.connect(partial(self.subcategoryClicked,iid,iname))
		      except:
			logging.exception("Exit Code = 7")
                cnt=cnt+1

	# if alphabetical choice was chosen then sets scroll bar positioning and clear chosen letter for not disturb next user requests
	if self.currentstartletter <> '':
		scrollBarItems.setValue((scrollarea.widget().height() / row) * (rowposition)) # (whole height * (matched row/number of rows)) - needed to change lightly this expression because python truncate result value when making a division between integers
		self.currentstartletter= ''

    #Call this method everytime a check box from search section is checked or unchecked
    def filterSearch(self,checkbox,catid):
      try:
        if checkbox.isChecked():
            self.searchArray.append(catid)
        else:
            self.searchArray.remove(catid)
        self.searchItems(self.searchLine, self.searchArray)
      except:
	logging.exception("CM Error Code = 11")

    #Used to search items based on a string typed in the textbox of Search Section
    def searchItems(self,arg,parentarray=[]):
      try:
        #print self.searchArray
        self.searchLine=arg
        #strSearch=self.qle.text()
        self.itype="item"
        arg=str(arg)
        if arg:
              #QMessageBox.about(self, "Warning", "Please fill the search box")
              #return
              self.currentname="Search"
	      self.setMenuLabel("search")
              arg=self.cleanupString(arg)
              els=arg
              items= gdb.searchItemsFromParents(self.searchArray, NR_TOTAL,  self.minAgeSel, self.maxAgeSel, els, self.currentorder, self.searchrecent)
              self.currentID='search';
              self.clearContent()
              self.showContent(items)
	      # remove alphabetical bar from screen, once matching is being made by search
	      self.clearAlphabet()
      except:
	logging.exception("CM Error Code = 12")


    #Cleanup search string inputed in textbox from special characters. Only letters and numbers are allowed
    def cleanupString(self, line=None):
        if line==None: return
        invalid = invalid = ['!','"','#','$','%','&','\\','(',')','*','+',',','-','.','/'
                    ,':',';','<','=','>','?','@','[',"'",']','^','`','{','|','}','~', ' ']
        for c in invalid: 
            if len(line)>0: line=line.replace(c,'_')
        return line


    #This method runs an items when it is chosen to be launched
    #Based on the item format and type, there are different applications for opening them
    #Behind are specified the most common types and softwares
    def itemClicked(self,itemid,file_path,parentcatname):
      try:
	#Disable GUI just before to run the item that was clicked, so user cannot launch other events while the item that he/she launched before is running
	window.setEnabled(False)

	start = time.time() # register start of use for calculate time of use
	if file_path.split('.')[-1].strip().upper() in ('HTM', 'HTML'):
		p = subprocess.call(['sensible-browser', file_path]) # if it is a .htm(l) document then calls default browser to open it
        elif file_path[-3:].upper() == 'ZIM': #if it is a zim file, then call kiwix to execute it
                p = subprocess.call(['kiwix', file_path])
        elif file_path[-3:].upper() == 'PDF': #if it is a pdf file, then call evince to open it
		p = subprocess.call(['evince',  '-s', '-p' , '1',  file_path]) #open presentation mode (-s); on the first page (-p 1)
		#p = subprocess.call(['evince',  '-s', file_path]) #open presentation mode (-s)
        elif file_path.split("/")[-1].split(' ')[0] == "libreoffice": # if it is an office application
		args=file_path.split("/")[-1].split(' ')
	        p=subprocess.Popen([ args[0], args[1] ])
	elif file_path[-3:].upper() == 'SWF': # if extension is swf then it is a flash file - so calls GNU flash player "gnash" to execute it
		p = subprocess.call(['gnash', file_path])
	elif parentcatname=="Videos":
		p = subprocess.call(['totem', file_path]) # if it is a video then calls "totem" video player to execute it
	# If item doesn't belong to a particular file type and not is a video then process as executable
	else:
		p = subprocess.call(file_path) # if it is a executable, call it directly

	end = time.time() # register end of use for calculate time of use

	gdb.updateItemUsage(int(itemid),end - start)  # updates last access, number of access and time of use

	#This command is needed to make mouse clicks or keyboard press triggered by user while Content Manager was disabled be processed before Content Manager be renabled. Otherwise they would be processed after Content Manager is renabled and would be processed undesirable
	QCoreApplication.processEvents()

	#Renable GUI after item finished its processing
	window.setEnabled(True)
      except:
	logging.exception("CM Error Code = 13")


    #order search result based on last access time.
    #This option gets the most recent results matching the search criterias
    def recentSearch(self,checkbox):
      try:
        if checkbox.isChecked():
           self.searchrecent=True
        else:
           self.searchrecent=False
        self.searchItems(self.searchLine, self.searchArray)
      except:
        logging.exception("CM Error Code = 14")


    #Generate the bar responsibile for alphabetical bar when a subcategory is chosen
    def alphabetDisplay(self):
      try:
        Alphabet_group = QButtonGroup()# Alphabet  group
        alphaButton = {}
        for i in range(27):
            label = chr(ord('A') + i)
            if label=="[":
               label="All"
            alphaButton[i] = QToolButton()
            alphaButton[i].setText(label)
            alphaButton[i].setObjectName("AlphaButton")
            alphaButton[i].clicked.connect(partial(self.alphabeticalChoice,label))
            #vbox.addWidget(QPushButton(label))
            alphabet_search.addWidget(alphaButton[i])
            Alphabet_group.addButton(alphaButton[i])
            alphabet_search.setSpacing(0);
            #Placing or seting the grouped ages on the main class object
            #self.setLayout(self.vlayout)
      except:
        logging.exception("CM Error Code = 15")


    #Call the method to search for items starting with the letter seletec in alphabetical bar
    def alphabeticalChoice(self,arg):
      try:
        #items= gdb.getItems( 5, 9, 0,100, arg, True)
        catlist=[self.currentID]
        if arg == "All":
	  self.currentstartletter=''
        else:
	  self.currentstartletter=arg

        self.clearContent()
        self.showContent(self.items) # doesn't access database - instead, use last retrieved items from subcategoryclicked method
      except:
        logging.exception("CM Error Code = 15")


    #Refresh the view during navigation
    def clearContent(self):
        for i in reversed(range(display_area.count())):
            display_area.itemAt(i).widget().deleteLater()

    def clearAlphabet(self):
        for i in reversed(range(alphabet_search.count())):
            alphabet_search.itemAt(i).widget().deleteLater()

    def clearSearch(self):
	self.qle.clear()
        for i in self.searchcheckbutton:
	    self.searchcheckbutton[i].setChecked(True) # categories are checked by default
	self.searchcheckbutton[i].setChecked(False) # recent is unchecked by default
	self.searchArray=list(self.searchArrayDefault)
	self.searchLine=''
	self.searchrecent=False

    #when ordering combo item is changed, stores it in a property and recalla method to reload screen with a new ordering
    def combochanged(self, text):
        if comboorder.currentIndex()==0:
		self.currentorder=RECENT_ORDER
	elif comboorder.currentIndex()==1:
		self.currentorder=FAVORITES_ORDER
        elif comboorder.currentIndex()==2:
		self.currentorder=ALPHA_ORDER
	else:
		self.currentorder=ALPHA_ORDER

        if self.currentID == "search":
		self.searchItems(self.searchLine, self.searchArray)
	else:
		self.subcategoryClicked(self.currentID,'',True)

  if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet(css)
    clicked =  pyqtSignal()
    #Create the signal mapper.
    signalMapper = QSignalMapper()
    #area of showing subcategories and items
    display_area = QGridLayout()

    #inserts scroll area for display area, so if there are some many item, there will be a scroll bar for them
    scrollarea=QScrollArea()
    scrollBarItems = scrollarea.verticalScrollBar()  # gets scroll bar to be handled in alphabetical choice
    w=QWidget()        
    vbox=QVBoxLayout(w)
    vbox.addLayout(display_area)
    scrollarea.setWidget(w)
    w.setGeometry(0, 0, 0, 0)
    scrollarea.setWidgetResizable(True)
    display_area_scroll = QGridLayout()
    display_area_scroll.addWidget(scrollarea)

    #area of left menu
    menu_layout=QHBoxLayout()

    #current menu bar that shows where we are in navigation
    menubar=QHBoxLayout()
	
	#The undo
    #undo_button=QPushButton(QIcon(IMAGE_PATH+'Undo.ico'),"Undo >")
    #undo_button.setIconSize(QSize(8,8))
    #undo_button.setObjectName("undo")
    #menubar.addWidget(undo_button)

    #label 'You are here' fixed text
    youarehere=QLabel("You are here > ")
    youarehere.setObjectName("youAreHere")
    menubar.addWidget(youarehere)

    #Path for where is navigation
    currentmenu=QLabel()
    currentmenu.setObjectName("currentMenu")
    menubar.addWidget(currentmenu, 1) #puts second parameter (scretch) as 1 to push order by combo to right

    #label order fixed text
    labelorder=QLabel("Order by")
    labelorder.setObjectName("labelOrder")
    menubar.addWidget(labelorder)

    #combo for ordering items - using model approach
    comboorder = QtGui.QComboBox()
    listorder = [
       QStandardItem ("Latest viewed"),
       QStandardItem ("Most popular"),
       QStandardItem ("Alphabetically")
       ]
    model = QStandardItemModel()
    model.appendColumn(listorder)
    comboorder.setModel(model)
    menubar.addWidget(comboorder)

    #area of alphabetical search
    alphabet_search=QVBoxLayout()
    #area of the main view (right content) including current menu bar,
    # items (subcategories) to be displyes and alphabetical search bar
    allcontent=QGridLayout()
    allcontent.addLayout(menubar,0,0,1,1)
    allcontent.addLayout(display_area_scroll,1,0,5,4)
    allcontent.addLayout(alphabet_search,0,10,5,1)
    #put menu and all content inside a same area
    main_layout=QHBoxLayout()
    main_layout.addLayout(menu_layout)
    main_layout.addLayout(allcontent)
    main_layout.setContentsMargins(0, 0, 0, 0)
    #area of showing the title bar: logo + age group + shutdown button
    titleBar=QWidget()
    titleBar.setObjectName("titleBar")
    #connent to database: create a new instance
    gdb=db4kids()
    #create a frame - it is needed do it before runContentManager for use window in runContentManager logic
    window = QFrame()
    #runContentManager class will call all the functions for generating the view and actions of ContentManager
    runContentManager()
    #put title and main content of ContentManager in a same area: QVBoxLayout
    mycontent=QVBoxLayout()
    mycontent.addWidget(titleBar)
    mycontent.addLayout(main_layout)
    mycontent.setContentsMargins(0, 0, 0, 0)
    #Put all the content generated above and show this frame
    window.setWindowFlags(Qt.FramelessWindowHint);
    window.setLayout(mycontent)
    window.setFixedWidth(screen_width)
    window.setFixedHeight(screen_height)
    #window.showNormal()
    window.showFullScreen()
    #window.showMaximized()  #shows full screen
    app.exec_()

except:
    logging.exception("CM Error Code = 2")
    sys.exit(2)
