#!/bin/bash

# name: games_installation_part3.sh
#       it is based on Fernando's games_installation.sh
# author(modifier): Ferincz Janos <janos.ferincz@gmail.com>
# date: 07/17/2014
# description: installs games for PCs4Kids Content Manager, makes links for each of them in games folder (/home/pcs4kids/pcs4kids/resources/game) and in icons folder (/home/pcs4kids/pcs4kids/images)
# requirements: it needs to be executed as root, because of required games installation permissions

HOME4KIDS=/srv/ContentManager
GAMEDIR=$HOME4KIDS/resources/doc
IMAGEDIR=$HOME4KIDS/images

ARCH=`uname -r | cut -f 3 -d "-"`

# Games installation

#kiwix: it has already been install by hand 
#echo "deb http://www.robertix.com/telechargements/binaires/ ./" >> /etc/apt/sources.list
#apt-get update
#apt-get install kiwix

#ooo4kids
wget  http://sourceforge.net/projects/educooo/files/OOo4Kids/Linux/deb/dists/testing/main/binary-$ARCH/ooo4kids-en-us_1.3-1_$ARCH.deb
dpkg -i ooo4kids-en-us_1.3-1_$ARCH.deb
rm  ooo4kids-en-us_1.3-1_$ARCH.deb

# Links creation for games
cd $GAMEDIR

ln -s /usr/bin/kiwix kiwix
#ln -s /usr/bin/ooo4kids1.3 ooo4kids
ln -s /usr/lib/ooo4kids-1.3/program/soffice ooo4kids


# Links creation for images
cd $IMAGEDIR

ln -s /usr/share/icons/kiwix/32x32/kiwix.png kiwix.png
ln -s /usr/share/icons/hicolor/32x32/apps/ooo4kids13-ooo4kids.png ooo4kids.png

