#!/bin/bash
# name: games_installation.sh
# author: Janos Ferincz
# date: 27/10/2014
# description: set the soft links of LibreOffice icons  for PCs4Kids Content Manager, 
# makes  links for each of them in image folder (/srv/ContentManager/image)
# requirements:

#PCS4KIDS=/home/jani/pcs4kids
PCS4KIDS=/srv/ContentManager

#set soft links to icons of the LibreOffice applications 
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-writer.png $PCS4KIDS/images/libreoffice-writer.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-draw.png $PCS4KIDS/images/libreoffice-draw.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-impress.png $PCS4KIDS/images/libreoffice-impress.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-calc.png $PCS4KIDS/images/libreoffice-calc.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-base.png $PCS4KIDS/images/libreoffice-base.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-math.png $PCS4KIDS/images/libreoffice-math.png

