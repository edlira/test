#!/bin/bash
# name: games_installation.sh
# author: Janos Ferincz
# date: 18/10/2014
# description: books in the books directory for PCs4Kids Content Manager, makes links for each of them in doc folder (/srv/ContentManager/resources/doc) 
# requirements:

#RES=/home/jani/pcs4kids/resources
RES=/srv/ContentManager/resources

ln -s $RES/books/abroad.pdf $RES/doc/abroad.pdf
ln -s $RES/books/adventureshuckle.pdf $RES/doc/adventureshuckle.pdf
ln -s $RES/books/adventuresoftoms.pdf $RES/doc/adventuresoftoms.pdf
ln -s $RES/books/beautythebeast.pdf $RES/doc/beautythebeast.pdf
ln -s $RES/books/books.awk $RES/doc/books.awk
ln -s $RES/books/books.sql $RES/doc/books.sql
ln -s $RES/books/books.txt $RES/doc/books.txt
ln -s $RES/books/cinderella.pdf $RES/doc/cinderella.pdf
ln -s $RES/books/goodytwoshoes.pdf $RES/doc/goodytwoshoes.pdf
ln -s $RES/books/mothergoosesnurseryrhymes.pdf $RES/doc/mothergoosesnurseryrhymes.pdf
ln -s $RES/books/oldfrenchfairytales.pdf $RES/doc/oldfrenchfairytales.pdf
ln -s $RES/books/poemsmychildrenlove.pdf $RES/doc/poemsmychildrenlove.pdf
ln -s $RES/books/simplepoemsfor.pdf $RES/doc/simplepoemsfor.pdf
ln -s $RES/books/sleepingbeauty.pdf $RES/doc/sleepingbeauty.pdf
ln -s $RES/books/snowimagechildish.pdf $RES/doc/snowimagechildish.pdf
ln -s $RES/books/storyofdoctordolittle.pdf $RES/doc/storyofdoctordolittle.pdf
ln -s $RES/books/wonderfulwizard.pdf $RES/doc/wonderfulwizard.pdf
