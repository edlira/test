#!/bin/bash

# name: games_installation_part2.sh
#       it is based on Fernando's games_installation.sh
# author(modifier): Ferincz Janos <janos.ferincz@gmail.com>
# date: 07/03/2014
# description: installs games for PCs4Kids Content Manager, makes links for each of them in games folder (/home/pcs4kids/pcs4kids/resources/game) and in icons folder (/home/pcs4kids/pcs4kids/images)
# requirements: it needs to be executed as root, because of required games installation permissions
#               +
#               Before installing ooo4kids-en-us, you should add "deb http://deb.ooo4kids.org testing main" to /etс/apt/sources.list
#               then run (update repository):
#               apt-get update
#               W: Failed to fetch http://deb.ooo4kids.org/dists/testing/Release.gpg  Could not resolve 'deb.ooo4kids.org'
#               --> ooo4kids-en-us IS NOT INSTALLED 

HOME4KIDS=/srv/ContentManager
GAMEDIR=$HOME4KIDS/resources/game
IMAGEDIR=$HOME4KIDS/images

# Games installation

#apt-get install childsplay
#apt-get install funnyboat
#apt-get install gcompris
#apt-get install gtans
#apt-get install hex-a-hop
#apt-get install jigzo
#apt-get install jumpnbump
#apt-get install koules
#apt-get install lmemory
#apt-get install madbomber
#apt-get install ooo4kids-en-us
#apt-get install supertuxkart
#apt-get install tuxmath
#apt-get install tuxpaint
#apt-get install tuxtype
#apt-get install vodovod

apt-get install childsplay funnyboat gcompris gtans hex-a-hop jigzo jumpnbump koules lmemory madbomber supertuxkart tuxmath tuxpaint tuxtype vodovod
#apt-get install ooo4kids-en-us

# Links creation for games
cd $GAMEDIR

ln -s /usr/games/childsplay childsplay
ln -s /usr/games/funnyboat funnyboat
ln -s /usr/games/gcompris gcompris
ln -s /usr/games/gtans gtans
ln -s /usr/games/hex-a-hop hex-a-hop
ln -s /usr/games/jigzo jigzo
ln -s /usr/games/jumpnbump jumpnbump
ln -s /usr/games/koules koules
ln -s /usr/games/lmemory lmemory
ln -s /usr/games/madbomber madbomber
#ln -s /usr/games/ooo4kids-en-us ooo4kids-en-us
#ln -s /usr/bin/ooo4kids-en-us ooo4kids-en-us
ln -s /usr/games/supertuxkart supertuxkart
ln -s /usr/games/tuxmath tuxmath
ln -s /usr/bin/tuxpaint tuxpaint
ln -s /usr/games/tuxtype tuxtype
ln -s /usr/games/vodovod vodovod

# Links creation for images
cd $IMAGEDIR

ln -s /usr/share/childsplay_sp/SPData/menu/default/logo_cp_32x32.png childsplay.png
ln -s /usr/share/app-install/icons/funnyboat.xpm funnyboat.xpm
ln -s /usr/share/app-install/icons/gcompris.png gcompris.png
ln -s /usr/share/icons/hicolor/32x32/apps/gtans.png gtans.png
ln -s /usr/share/app-install/icons/hex-a-hop.xpm hex-a-hop.xpm
ln -s /usr/share/app-install/icons/jigzo.xpm jigzo.xpm
ln -s /usr/share/pixmaps/jumpnbump.xpm jumpnbump.xpm
ln -s /usr/share/app-install/icons/koules.png koules.png
ln -s /usr/share/app-install/icons/lmemory.xpm lmemory.xpm
ln -s /usr/share/app-install/icons/madbomber-icon.xpm  madbomber.xpm
ln -s /usr/share/icons/hicolor/32x32/apps/supertuxkart.xpm supertuxkart.xpm
#ln -s /usr/share/app-install/icons/tuxmath.svg tuxmath.svg
ln -s /usr/share/tuxmath/images/icons/tuxmath.ico tuxmath.ico
ln -s /usr/share/icons/hicolor/32x32/apps/tuxpaint.png tuxpaint.png
ln -s /usr/share/tuxtype/images/icons/tuxtype.ico  tuxtype.ico
ln -s /usr/share/games/vodovod/vodovod.png vodovod.png
#ln -s /usr/share/app-install/icons/vodovod.xpm vodovod.xpm


