# name: games_installation.sh
# author: Fernando Barreto de Almeida
# date: 06/29/2014
# description: installs games for PCs4Kids Content Manager, makes links for each of them in games folder (/srv/ContentManager/resources/game) and in icons folder (/srv/ContentManager/images)
# requirements: it needs to be executed as root, because of required games installation permissions

# Games installation
apt-get install libreoffice
apt-get install midori
apt-get install kalgebra
apt-get install kalzium
apt-get install kanagram
apt-get install kbruch
apt-get install kgeography
apt-get install khangman
apt-get install kig
apt-get install kiten
apt-get install kmplot
apt-get install kstars
apt-get install ktouch
apt-get install kturtle
apt-get install kwordquiz
apt-get install marble
apt-get install parley
apt-get install frozen-bubble
apt-get install lincity-ng
apt-get install freeciv-client-gtk
apt-get install pingus
apt-get install supertux
apt-get install gimp
apt-get install inkscape
apt-get install holotz-castle

# Links creation for games
cd /srv/ContentManager/resources/game

ln -s /usr/lib/libreoffice/program/soffice libreoffice
ln -s /usr/bin/midori midori
ln -s /usr/bin/kalgebra kalgebra
ln -s /usr/bin/kalzium kalzium
ln -s /usr/bin/kanagram kanagram
ln -s /usr/bin/kbruch kbruch
ln -s /usr/bin/kgeography kgeography
ln -s /usr/bin/khangman khangman
ln -s /usr/bin/kig kig
ln -s /usr/bin/kiten kiten
ln -s /usr/bin/kmplot kmplot
ln -s /usr/bin/kstars kstars
ln -s /usr/bin/ktouch ktouch
ln -s /usr/bin/kturtle kturtle
ln -s /usr/bin/kwordquiz kwordquiz
ln -s /usr/bin/marble marble
ln -s /usr/bin/parley parley
ln -s /usr/games/frozen-bubble frozen-bubble
ln -s /usr/games/lincity-ng lincity-ng
ln -s /usr/games/freeciv-gtk2 freeciv-gtk2
ln -s /usr/games/pingus pingus
ln -s /usr/games/supertux supertux
ln -s /usr/bin/gimp gimp
ln -s /usr/bin/inkscape inkscape
ln -s /usr/games/holotz-castle holotz-castle

# Links creation for images
cd /srv/ContentManager/images

ln -s /usr/share/icons/gnome/32x32/categories/applications-science.png applications-science.png
ln -s /usr/share/icons/gnome/32x32/categories/applications-graphics.png applications-graphics.png

ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-main.png libreoffice-main.png
ln -s /usr/share/icons/hicolor/32x32/apps/midori.png midori.png
ln -s /usr/share/icons/hicolor/32x32/apps/kalgebra.png kalgebra.png
ln -s /usr/share/icons/hicolor/32x32/apps/kalzium.png kalzium.png
ln -s /usr/share/icons/hicolor/32x32/apps/kanagram.png kanagram.png
ln -s /usr/share/icons/hicolor/32x32/apps/kbruch.png kbruch.png
ln -s /usr/share/icons/hicolor/32x32/apps/kgeography.png kgeography.png
ln -s /usr/share/icons/hicolor/32x32/apps/khangman.png khangman.png
ln -s /usr/share/icons/hicolor/32x32/apps/kig.png kig.png
ln -s /usr/share/icons/hicolor/32x32/apps/kiten.png kiten.png
ln -s /usr/share/icons/hicolor/32x32/apps/khangman.png khangman.png
ln -s /usr/share/icons/hicolor/32x32/apps/kmplot.png kmplot.png
ln -s /usr/share/icons/hicolor/32x32/apps/kstars.png kstars.png
ln -s /usr/share/icons/hicolor/32x32/apps/ktouch.png ktouch.png
ln -s /usr/share/icons/hicolor/32x32/apps/kturtle.png kturtle.png
ln -s /usr/share/icons/hicolor/32x32/apps/kwordquiz.png kwordquiz.png
ln -s /usr/share/icons/hicolor/32x32/apps/marble.png marble.png
ln -s /usr/share/icons/hicolor/32x32/apps/parley.png parley.png
ln -s /usr/share/games/frozen-bubble/icons/frozen-bubble-icon-32x32.png frozen-bubble-icon-32x32.png
ln -s /usr/share/app-install/icons/lincity-ng.png lincity-ng.png
ln -s /usr/share/icons/hicolor/32x32/apps/freeciv-client.png freeciv-client.png
ln -s /usr/share/games/pingus/data/images/icons/pingus-icon.png pingus-icon.png
ln -s /usr/share/games/supertux/images/icon.xpm icon.xpm
ln -s /usr/share/icons/hicolor/32x32/apps/gimp.png gimp.png
ln -s /usr/share/icons/hicolor/32x32/apps/inkscape.png inkscape.png
ln -s /usr/share/games/holotz-castle/game/icon/icon.bmp icon.bmp
