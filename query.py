#!/usr/bin/env python
 
#-------------------------------
# The query.py file contains the functions that retrieve and modifiy data in the database of Content Manager. These function are embedded into the db4kids class (member funtions).
# The db4kids class also create the connection with the pcs4kids database using an encrypted password.
# 
# Some general (or important) traits:
# * In general the functions retrieving data from the database give back a tuple (instead of list).  
#   You can reach the elements of a tuple like elements of a list but you can not modify them in a tuple (by chance...).
#   we retrieve database data into lists then convert them into tuples before returning (elements become constant).
# * using oracle-like variable binding in SQLs
# * search, where we use it in the functions,  is NOT case-sensitive.
# * take care the choice parameter. in some function (getItemsFromCatChosen, searchItemsFromParents) it must be a list  (even if you would like to choose only one category - e.g.: [7], not 7) 
# 
# (the member) functions of the db4kids class:
#-------------------------------
import sys

from PyQt4.QtSql import *
from PyQt4.QtCore import (QDate, QDateTime, QFile, QString, QVariant, Qt,SIGNAL)
from config import *     

# added by Fernando in 04/21/2014 for database password encryption
from Crypto.Cipher import DES
import binascii
# end of added by Fernando in 04/21/2014 for database password encryption

import logging
import inspect

LOGFILENAME = "logs/query.log"
#FORMAT = " %(levelname) -8s - %(asctime)-15s  - %(funcName)s - %(message)s"
FORMAT = " %(levelname) -6s - %(asctime)-15s  %(message)s"


LEVEL = logging.INFO
if DEVELOPER:
#if True:
    LEVEL = logging.DEBUG

logging.basicConfig(filename=LOGFILENAME, filemode ='w',level=LEVEL, format = FORMAT)
#logging.basicConfig(filename='../example.log',filemode ='w',level=LEVEL, format = FORMAT)

class db4kids(object):

# added and changed by Fernando in 04/21/2014 for database password encryption	
	obj=DES.new('abcdefgh', DES.MODE_ECB)
	dbpass_decrypt = obj.decrypt(binascii.a2b_uu(dbpass));
	dbpass_decrypt = dbpass_decrypt[2:int(dbpass_decrypt[:2]) + 2]
	def __init__(self, DBNAME = dbname, USER = dbuser, PASSWD = dbpass_decrypt):
# end of added and changed by Fernando in 04/21/2014 for database password encryption
           try:
		self.db = QSqlDatabase.addDatabase("QMYSQL")
		self.db.setDatabaseName(DBNAME)
		self.db.setUserName(USER);
		self.db.setPassword(PASSWD);

		if not self.db.open():
                        logging.critical("Database: " + DBNAME + ", User: " + USER + " --> DB connection is NOKe :(" ) 
			#logging.error(db.lastError().text())
        		sys.exit(1)
		else:
                        logging.info("Database: " + DBNAME + ", User: " + USER + " --> DB connection is OK :)" ) 

		self.query = QSqlQuery()
           except:
		logging.exception("CM Error Code = db1")
		sys.exit(1)

        #-------------------------------
        #Name: getItems
        #Description: This method retrieves the most popular and (the most) recently accessed items from the database (which match for the filter conditions: limit, age, ..)
        # @limit_numberaccess: Type: Integer, total number of result of the most accessed/popular items (the upper part of the union) 
        # @limit_numberaccess: Type: Integer, total number of result of the last accessed items (the below part of the union)
        # @minAgeSel: Type Integer, minimun age in the allowed range of age
        # @maxAgeSel: Type Integer, maximum age in the allowed range of age
        # 
        # return value: tuple (tuple of tuples of resultset rows) instead of list of lists (The cause is that the elements of tuple can not be modified)
        #-------------------------------
	def getItems(self, limit_numberaccess = LIMIT_NUMBERACCESS, limit_lastaccess = LIMIT_LASTACCESS, minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE):
           try:
                logging.info("Calling %s(%s, %s, %s, %s)" , "getItems", limit_numberaccess, limit_lastaccess, minAgeSel , maxAgeSel)

		PARENTCATEGORY_ID, PARENTCATEGORY_NAME, PARENTCATEGORY_ICONFILENAME, CATEGORY_ID, CATEGORY_NAME, CATEGORY_ICONFILENAME,\
                ITEM_ID, ITEM_NAME, ITEM_ICONFILENAME, ITEM_FULLPATH, ITEM_MINIMUMAGE, ITEM_MAXIMUMAGE = range(12)

		# It is a union of 2 queries
		# First one loads most accessed items (favorites) - Favorites are measured by total time of use divided by number of accesses
		# Second one loads last accessed items (recents)
		# Half of screen (5) is filled with favorites, so maximum number of records of first query is 5
		# The rest of screen (4) is filled with recent. As if a record is shown in both queries it is just loaded one time, maximum number of
		# records of second query is set to 9 (the whole size of screen), so in case of all records from first query are also in second query
		# 9 records are loaded anyway. If more than 9 are loaded, showContent() method discards unnecessary records.

		self.query.prepare("""(select
                                             c1.category_id as parentcategory_id,
                                             c1.category_name as parentcategory_name, 
                                             c1.category_iconfilename as parentcategory_iconfilename,
                                             c2.category_id,
                                             c2.category_name , 
                                             c2.category_iconfilename,
                                             i.item_id,
                                             i.item_name,
                                             i.item_iconfilename,
                                             concat(c1.category_path, i.item_filename) as item_fullpath,
                                             i.item_minimumage,
                                             i.item_maximumage
                                        from category c1, 
                                             category c2, 
                                             item i 
                                       where c1.category_id = c2.category_parentid 
                                         and i.category_id = c2.category_id 
                                         and c1.category_status = 1
                                         and c2.category_status = 1
                                         and i.item_status = 1
                                         and greatest(:minage1, i.item_minimumage) <= least(:maxage1, i.item_maximumage)
                                       order by (case when ifnull(i.item_numberaccess, 0) = 0 then 0 else (i.item_usedtime/i.item_numberaccess) end) desc, i.item_lastaccess desc
			               limit :limit_numberaccess)
				      union
				      (select
                                             c1.category_id as parentcategory_id,
                                             c1.category_name as parentcategory_name,
                                             c1.category_iconfilename as parentcategory_iconfilename,
                                             c2.category_id,
                                             c2.category_name ,
                                             c2.category_iconfilename,
                                             i.item_id,
                                             i.item_name,
                                             i.item_iconfilename,
                                             concat(c1.category_path, i.item_filename) as item_fullpath,
                                             i.item_minimumage,
                                             i.item_maximumage
                                        from category c1,
                                             category c2,
                                             item i
                                       where c1.category_id = c2.category_parentid
                                         and i.category_id = c2.category_id
                                         and c1.category_status = 1
                                         and c2.category_status = 1
                                         and i.item_status = 1
                                         and greatest(:minage2, i.item_minimumage) <= least(:maxage2, i.item_maximumage)
                                       order by i.item_lastaccess desc, (case when ifnull(i.item_numberaccess, 0) = 0 then 0 else (i.item_usedtime/i.item_numberaccess) end) desc
			               limit :limit_lastaccess)
				  """)

		self.query.bindValue(":limit_numberaccess", QVariant(int(limit_numberaccess)))
		self.query.bindValue(":limit_lastaccess", QVariant(int(limit_lastaccess)))
		self.query.bindValue(":minage1", QVariant(int(minAgeSel)))
		self.query.bindValue(":minage2", QVariant(int(minAgeSel)))
		self.query.bindValue(":maxage1", QVariant(int(maxAgeSel)))
		self.query.bindValue(":maxage2", QVariant(int(maxAgeSel)))

		ret = []
		if self.query.exec_():
       			while self.query.next():
				row = []
                                row.append(unicode(self.query.value(ITEM_ID).toInt()[0]))
                                row.append(unicode(self.query.value(ITEM_NAME).toString()))
                                row.append(unicode(self.query.value(ITEM_ICONFILENAME).toString()))
                                row.append(unicode(self.query.value(ITEM_FULLPATH).toString()))
               			row.append(unicode(self.query.value(PARENTCATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(PARENTCATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(PARENTCATEGORY_ICONFILENAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(CATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ICONFILENAME).toString()))
               			row.append(unicode(self.query.value(ITEM_MINIMUMAGE).toInt()[0]))
               			row.append(unicode(self.query.value(ITEM_MAXIMUMAGE).toInt()[0]))

				ret.append(tuple(row))

                logging.debug("getItems() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getItems() - %3d row(s) returned.", len(ret))

		return tuple(ret)
           
           except:
		logging.exception("CM Error Code = db2")
        
        #-------------------------------
        #Name: getCategories
        #Description: This method retrieves all categories from the database which have items matching for the filter condition (enabled, age)
        # @limit: Type: Integer, total number of result
        # @minAgeSel: Type Integer, minimun age in the allowed range of age
        # @maxAgeSel: Type Integer, maximum age in the allowed range of age
        #
        # return value:  tuple (tuple of tuples of resultset rows)
        #-----------------------------
	def getCategories(self, limit = NR_TOTAL, minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE):
           try:

                logging.info("Calling %s(%d, %d, %d)" , "getCategories", limit,  minAgeSel, maxAgeSel)
		PARENTCATEGORY_ID, PARENTCATEGORY_NAME, PARENTCATEGORY_ICONFILENAME, CATEGORY_ID, CATEGORY_NAME, CATEGORY_ICONFILENAME = range(6)

		self.query.prepare("""select 
                                      c1.category_id as parentcategory_id,
                                      c1.category_name as parentcategory_name, 
                                      c1.category_iconfilename as parentcategory_iconfilename,
                                      c2.category_id,
                                      c2.category_name , 
                                      c2.category_iconfilename
				 from category c1, 
        		     	      category c2 
		        	where c1.category_id = c2.category_parentid 
                                 and c1.category_status = 1
                                 and c2.category_status = 1
                                 and exists ( select 'OK' from item i
                                              where c2.category_id = i.category_id 
                                                and i.item_status = 1
                                                and greatest(:minage, i.item_minimumage) <= least(:maxage, i.item_maximumage))
				order by c2.category_name
        			limit :limit;""")
	
		self.query.bindValue(":limit", QVariant(int(limit)))
		self.query.bindValue(":minage", QVariant(int(minAgeSel)))
		self.query.bindValue(":maxage", QVariant(int(maxAgeSel)))

		ret = []
		if self.query.exec_():
        		while self.query.next():
				row = []
               			row.append(unicode(self.query.value(PARENTCATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(PARENTCATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(PARENTCATEGORY_ICONFILENAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(CATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ICONFILENAME).toString()))

				ret.append(tuple(row))

                logging.debug("getCategories() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getCategories() - %3d row(s) returned.", len(ret))
		return tuple(ret)

           except:
		logging.exception("CM Error Code = db3")

        #-------------------------------
        #Name: getParentCategories 
        #Description: This method retrieves all parent categories from the database
        # @limit: Type: Integer, total number of result
        #
        # return value:  tuple (tuple of tuples of resultset rows)
        #-----------------------------
	def getParentCategories (self, limit = NR_TOTAL):
           try:
                logging.info("Calling %s(%s)" , "getParentCategories", limit)
                CATEGORY_ID, CATEGORY_NAME, CATEGORY_ICONFILENAME = range(3)
  		self.query.prepare("""select c.category_id,
                                             c.category_name , 
                                             c.category_iconfilename
				from category c
				where c.category_parentid is Null
                                 and c.category_status = 1
				order by c.category_name
                                limit :limit;""")

		self.query.bindValue(":limit", QVariant(int(limit)))

		ret = []
		if self.query.exec_():
        		while self.query.next():
                                row = []
               			row.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(CATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ICONFILENAME).toString()))
		
				ret.append(tuple(row))

                logging.debug("getParentCategories() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getParentCategories() - %3d row(s) returned.", len(ret))
		return tuple(ret)

           except:
		logging.exception("CM Error Code = db4")

        #-------------------------------
        #Name: getCategoriesChosen
        #Description: This method retrieves all (sub)categories of a given parentcategory (determined by choice paraneter) from the database 
        #             which have items matching for the filter condition (enabled, age)
        # @choice: Type Integer, the value of category_parentid of the selected parent category
        # @limit: Type: Integer, total number of result
        # @minAgeSel: Type Integer, minimun age in the allowed range of age
        # @maxAgeSel: Type Integer, maximum age in the allowed range of age
        #
        # return value:  tuple (tuple of tuples of resultset rows)
        #-----------------------------
	def getCategoriesChosen(self, choice, limit = NR_TOTAL, minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE):
           try:
                logging.info("Calling %s(%d, %s, %s, %s)" , "getCategoriesChosen", choice, limit, minAgeSel, maxAgeSel)
                CATEGORY_ID, CATEGORY_NAME, CATEGORY_ICONFILENAME = range(3)
	
		catName = self.getNameOfCategoryId(choice).lower()
 
		self.query.prepare("""select c.category_id,
                                             c.category_name , 
                                             c.category_iconfilename
				from category c
				where c.category_parentid = :choice
                                  and c.category_status = 1
                                  and exists ( select 'OK' from item i
                                                where c.category_id = i.category_id 
                                                  and i.item_status = 1
                                                  and greatest(:minage, i.item_minimumage) <= least(:maxage, i.item_maximumage))
				order by c.category_name
                                limit :limit;""")

		self.query.bindValue(":choice", QVariant(choice))
		self.query.bindValue(":limit", QVariant(int(limit)))
		self.query.bindValue(":minage", QVariant(int(minAgeSel)))
		self.query.bindValue(":maxage", QVariant(int(maxAgeSel)))

		ret = [(unicode(choice),'All ' + catName, 'all.jpg')]
		if self.query.exec_():
        		while self.query.next():
                                row = []
               			row.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(CATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ICONFILENAME).toString()))
		
				ret.append(tuple(row))

                logging.debug("getCategoriesChosen() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getCategoriesChosen() - %3d row(s) returned.", len(ret))
		return tuple(ret)

           except:
		logging.exception("CM Error Code = db5")

	#------------------------------
	#Name: getItemsFromCatChosen
	#Description: This method retrieves all items of a given category (determined by choice paraneter) from the database 
	# @choice: Type list of Integer (!!!), the value of category_id of the selected category
	#          WARMING: You must use list even if you would like to choose only one category (e.g.: [6])
	# @limit: Type: Integer, total number of result
	# @minAgeSel: Type Integer, minimun age in the allowed range of age
	# @maxAgeSel: Type Integer, maximum age in the allowed range of age
	# @search_str: Type String, the string literal which we search in the name of items (item_name)
	# @orderby: Type Integer (ENUM), it cointains the ORDER BY type (whithout ORDER BY)  (e.g.: ALPHA_ORDER -> ORDER BY item_name)
	# @recentitems: Type Boolean. Indicates if just items accessed recently will be retrieved or not
	#
	# return value:  tuple (tuple of tuples of resultset rows)
	#-----------------------------
        def getItemsFromCatChosen(self, choice, limit = NR_TOTAL,  minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE, search_str = '', orderby = ALPHA_ORDER, recentitems = False):
           try:
                logging.info("Calling %s(%s, %s, %s, %s, '%s', '%s', '%s')" , "getItemsFromCatChosen", choice, limit,  minAgeSel, maxAgeSel, search_str, orderby, recentitems)
                PARENTCATEGORY_NAME, ITEM_ID, ITEM_NAME, ITEM_ICONFILENAME, ITEM_FULLPATH = range(5)

		# matches all items supplied by search parameter
		search_str = '%' + search_str.upper() + '%'

		#according to received parameter, determines ordering
                if orderby == RECENT_ORDER:
                  order_by = 'i.item_lastaccess desc, (case when ifnull(i.item_numberaccess, 0) = 0 then 0 else (i.item_usedtime/i.item_numberaccess) end) desc'
		elif orderby == FAVORITES_ORDER:
                  order_by = '(case when ifnull(i.item_numberaccess, 0) = 0 then 0 else (i.item_usedtime/i.item_numberaccess) end) desc, i.item_lastaccess desc'
		elif orderby == ALPHA_ORDER:
                  order_by = 'item_name'
		else:
		  order_by = 'item_name'

		if recentitems:
		  recentitems_clause = 'and datediff(curdate(), case when ifnull(i.item_lastaccess, 0) = 0 then ' + INTIAL_DATE + ' else i.item_lastaccess end) <= ' + RECENT_DAYS
		else:
		  recentitems_clause = ''

		# It is a union of 2 queries
                self.query.prepare("""select c1.category_name as parentcategory_name,
					     i.item_id,
                                             i.item_name,
                                             i.item_iconfilename,
                                             concat(c1.category_path, i.item_filename) as item_fullpath
                                        from category c1,
                                             category c2,
                                             item i
                                       where c1.category_id = c2.category_parentid
                                         and i.category_id = c2.category_id
                                         and c1.category_status = 1
                                         and c2.category_status = 1
                                         and i.item_status = 1
					 and (i.category_id in (###choice###) OR c1.category_id in (###choice###))
					 and upper(i.item_name) like :search
                                         and greatest(:minage, i.item_minimumage) <= least(:maxage, i.item_maximumage)
					 ###recentitems_clause###
                                    order by ###order_by###
                                    limit :limit;""".replace('###choice###', ",".join(str(c)for c in choice))
                                                    .replace('###order_by###', order_by)
						    .replace('###recentitems_clause###', recentitems_clause))

		self.query.bindValue(":search", QVariant(QString(search_str)))
		self.query.bindValue(":minage", QVariant(int(minAgeSel)))
		self.query.bindValue(":maxage", QVariant(int(maxAgeSel)))
                self.query.bindValue(":limit", QVariant(int(limit)))

                ret = []
                if self.query.exec_():

                        while self.query.next():

                                row = []
                                row.append(unicode(self.query.value(ITEM_ID).toInt()[0]))
                                row.append(unicode(self.query.value(ITEM_NAME).toString()))
                                row.append(unicode(self.query.value(ITEM_ICONFILENAME).toString()))
                                row.append(unicode(self.query.value(ITEM_FULLPATH).toString()))
				row.append(unicode(self.query.value(PARENTCATEGORY_NAME).toString()))
                                ret.append(tuple(row))

		logging.debug(unicode(self.query.lastQuery()).replace('###choice###', ",".join(str(c)for c in choice))
                    .replace('###order_by###', order_by)
                    .replace(':minage', str(minAgeSel))
                    .replace(':maxage', str(maxAgeSel))
                    .replace(':search', "'" + str(search_str) + "'")
                    .replace(':limit',str(limit)))

                logging.debug("getItemsFromCatChosen() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getItemsFromCatChosen() - %3d row(s) returned.", len(ret))
                return tuple(ret)
        
           except:
		logging.exception("CM Error Code = db6")

        #-------------------------------
        #Name: getCategoriesChosenWithParents
        #Description: This method retrieves all category chosen (also their parent category) from the database which have items matching for the filter condition (enabled, age)
        # @choice: Type Integer, the value of category_id of the selected parent category
        # @limit: Type: Integer, total number of result
        # @minAgeSel: Type Integer, minimun age in the allowed range of age
        # @maxAgeSel: Type Integer, maximum age in the allowed range of age
        #
        # return value:  tuple (tuple of tuples of resultset rows)
        #-----------------------------
        def getCategoriesChosenWithParents(self, choice, limit = NR_TOTAL, minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE):
           try:
                logging.info("Calling %s(%s, %s, %s,  %s)" , "getCategoriesChosenWithParents", choice, limit,  minAgeSel, maxAgeSel)
		PARENTCATEGORY_ID, PARENTCATEGORY_NAME, PARENTCATEGORY_ICONFILENAME, CATEGORY_ID, CATEGORY_NAME, CATEGORY_ICONFILENAME = range(6)

                self.query.prepare("""select c1.category_id as parentcategory_id,
                                             c1.category_name as parentcategory_name, 
                                             c1.category_iconfilename as parentcategory_iconfilename,
                                             c2.category_id,
                                             c2.category_name , 
                                             c2.category_iconfilename
                                from category c1, 
                                     category c2 
                                where c1.category_id = c2.category_parentid 
                                  and c1.category_status = 1
                                  and c2.category_status = 1
                                  and c1.category_id = :choice
                                  and exists ( select 'OK' from item i
                                                where c2.category_id = i.category_id 
                                                  and i.item_status = 1
                                                  and greatest(:minage, i.item_minimumage) <= least(:maxage, i.item_maximumage))
                                order by c2.category_name
                                limit :limit;""")

                self.query.bindValue(":limit", QVariant(int(limit)))
                self.query.bindValue(":choice", QVariant(int(choice)))
		self.query.bindValue(":minage", QVariant(int(minAgeSel)))
		self.query.bindValue(":maxage", QVariant(int(maxAgeSel)))

                ret = []
                if self.query.exec_():
                        while self.query.next():
                                row = []

               			row.append(unicode(self.query.value(PARENTCATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(PARENTCATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(PARENTCATEGORY_ICONFILENAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))
               			row.append(unicode(self.query.value(CATEGORY_NAME).toString()))
               			row.append(unicode(self.query.value(CATEGORY_ICONFILENAME).toString()))

                                ret.append(tuple(row))

                logging.debug("getCategoriesChosenWithParents() returns:\n" + str(tuple(ret)))
                logging.info("Exiting getCategoriesChosenWithParents() - %3d row(s) returned.", len(ret))
                return tuple(ret)
           except:
		logging.exception("CM Error Code = db7")

        #-------------------------------
        #Name: searchItemsFromParents
        #Description: This method retrieves all items of a given PARENTcategory (determined by choice paraneter) from the database (wrapper function of getItemsFromCatChosen)
        # @choice: Type list of Integer (!!!), the value of category_parentid of the selected parent category
        #          WARMING: You must use list even if you would like to choose only one category (e.g.: [6])
        # @limit: Type: Integer, total number of result
        # @minAgeSel: Type Integer, minimun age in the allowed range of age
        # @maxAgeSel: Type Integer, maximum age in the allowed range of age
        #
        # The next two parameters simplly are given forward to the getItemsFromCatChosen function (it is invoke in this function)
        # @search_str: Type String, the string literal which we search in the name of items (item_name)
	# @orderby: Type Integer (ENUM), it cointains the ORDER BY type (whithout ORDER BY)  (e.g.: ALPHA_ORDER -> ORDER BY item_name)
	# @recentitems: Type Boolean. Indicates if just items accessed recently will be retrieved or not
        #
        # return value:  tuple (tuple of tuples of resultset rows)
        #
        # Additional comment:
        # This function has two phases:
        # 1) retrieve all categories of the given parent_categories (determined by the choice parameter)
        # 2) invoke getItemsFromCatChosen with the 1) phase result set as its choice parameter 
        #-----------------------------
        def   searchItemsFromParents(self, choice,  limit = NR_TOTAL,  minAgeSel = LIMIT_MINAGE, maxAgeSel = LIMIT_MAXAGE, search_str = '', orderby = ALPHA_ORDER, recentitems = False):
           try:
                logging.info("Calling %s(%s, %s, %s, %s, '%s', '%s', '%s')" , " searchItemsFromParents", choice, limit,  minAgeSel , maxAgeSel, search_str, orderby, recentitems)
                search_str = '%' +search_str.upper() + '%'
                CATEGORY_ID = 0
                self.query.prepare("""select c.category_id
                                        from category c
                                       where c.category_parentid in (###choice###)
                                         and c.category_status = 1
                                    order by c.category_id""".replace('###choice###', ",".join(str(c) for c in choice)))

                ret = []
                if self.query.exec_():
                        while self.query.next():
                                ret.append(unicode(self.query.value(CATEGORY_ID).toInt()[0]))

                result =  self.getItemsFromCatChosen( ret, limit,  minAgeSel, maxAgeSel, search_str, orderby, recentitems)

                logging.debug("searchItemsFromParents() returns:\n" + str(tuple(result)))
                logging.info("Exiting  searchItemsFromParents() - %3d row(s) returned.", len(result))
                #return tuple(ret)
                return tuple(result)
           except:
		logging.exception("CM Error Code = db8")

        #-------------------------------
        #Name: updateItemUsage
        #Description: This method update the usage data (item_numberaccess , item_lastaccess, item_usedtime) of an item  in the item table
        # @choice: Type Integer, the value of item_id of an item whose usage has just been finished 
        #
        # return value:  NO return value (it is a procedure)
        #
        # add comm: the unit of item_usedtime is seconds (it seems to me :) )
        #-----------------------------
        def updateItemUsage(self, choice, usedtime):
           try:
                logging.info("Calling %s(%d, %d)" , " updateItemUsage", choice, usedtime) 

                self.query.prepare("""update item t
                                      set t.item_numberaccess = ifnull(t.item_numberaccess, 0) + 1, 
                                          t.item_lastaccess = NOW(), 
                                          t.item_usedtime = ifnull(t.item_usedtime, 0) + :usedtime
                                      where t.item_id = :choice""")
		

                self.query.bindValue(":choice", QVariant(int(choice)))
                self.query.bindValue(":usedtime", QVariant(int(usedtime)))

                self.query.exec_()
                logging.debug("updateItemUsage() %d row(s) affected." , self.query.numRowsAffected())
                logging.info("Exiting  updateItemUsage().")
           except:
		logging.exception("CM Error Code = db9")

        #-------------------------------
        #Name: getNameOfCategoryId
        #Description: This method retrieves the name of the given category (determined by the choice parameter) from the database 
        # @choice: Type Integer, the value of category_id of the selected  (parent or sub)category
        #
        # return value:  tuple (tuple of stri
        def getNameOfCategoryId(self, choice):
           try:
                logging.info("Calling %s(%d)" , " getNameOfCategoryId", choice) 

                CATEGORY_NAME = 0
                self.query.prepare("""select c.category_name
                                from category c
                                where c.category_id  = :choice
                                 and c.category_status = 1""")

                self.query.bindValue(":choice", QVariant(int(choice)))

                ret = ''
                if self.query.exec_():
                        while self.query.next():
                                ret = unicode(self.query.value(CATEGORY_NAME).toString())

                logging.debug("getNameOfCategoryId() returns: " + str(ret))
                logging.info("Exiting getNameOfCategoryId() - %3d row(s) returned.", self.query.size())
                return ret

           except:
                logging.exception("CM Error Code = db10")

 


        '''
	def getItemNameLimit(self, limit=NR_TOTAL):
        	ITEM_NAME = 0
		ret = []

		self.query.prepare("""select item_name from item
				order by item_numberaccess desc, item_lastaccess desc
				limit :limit;""")

		self.query.bindValue(":limit", QVariant(int(limit)))

		if self.query.exec_():
        		while self.query.next():
				ret.append(unicode(self.query.value(ITEM_NAME).toString()))

		return ret

        '''

