-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 27, 2015 at 11:36 PM
-- Server version: 5.5.40
-- PHP Version: 5.4.35-0+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pcs4kids`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `category_parentid` int(11) DEFAULT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_iconfilename` varchar(255) DEFAULT NULL,
  `category_path` varchar(255) DEFAULT NULL COMMENT 'Just filled for main categories.\nIt is the path where files from this category are.',
  `category_status` tinyint(4) NOT NULL COMMENT 'enabled or disabled',
  `category_creationuser` int(11) NOT NULL,
  `category_createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  KEY `fk_Category_Category1` (`category_parentid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores categories information. Categories are the main divis';

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_parentid`, `category_name`, `category_iconfilename`, `category_path`, `category_status`, `category_creationuser`, `category_createdtime`) VALUES
(1, NULL, 'Games', 'gamepad.png', 'resources/game/', 1, 1, '2015-07-12 12:13:23'),
(2, NULL, 'Videos', 'videos.gif', 'resources/videos/', 1, 1, '2014-10-19 21:33:51'),
(3, NULL, 'Documents', 'documents.gif', 'resources/doc/', 1, 1, '2014-10-19 21:34:07'),
(4, 1, 'Action', 'action.png', NULL, 1, 1, '2014-07-15 21:41:09'),
(5, 1, 'Adventure', 'adventure.png', NULL, 1, 1, '2012-02-02 16:05:01'),
(6, 1, 'Geography', 'geography.png', NULL, 1, 1, '2012-02-03 16:05:01'),
(7, 1, 'Educational', 'educational.png', NULL, 1, 1, '2012-02-04 16:05:01'),
(8, 1, 'Multiplayer', 'multiplayer.png', NULL, 1, 1, '2014-07-15 21:41:09'),
(9, 1, 'Painting', 'painting.png', NULL, 0, 1, '2014-09-30 23:10:46'),
(10, 1, 'Puzzle', 'puzzle.png', NULL, 1, 1, '2014-07-15 21:41:09'),
(11, 1, 'Graphics', 'applications-graphics.png', NULL, 1, 1, '2014-07-15 20:57:17'),
(12, 1, 'Strategy', 'strategy.png', NULL, 1, 1, '2012-02-04 16:05:01'),
(13, 1, 'Sports', 'sports.png', NULL, 1, 1, '2012-02-05 16:05:01'),
(15, 3, 'Encyclopedia', 'books.png', NULL, 1, 1, '2014-09-30 22:10:15'),
(16, 3, 'English', 'english.png', NULL, 1, 1, '2014-07-15 21:04:16'),
(17, 3, 'Books', 'books.png', NULL, 1, 1, '2014-09-30 22:19:00'),
(18, 3, 'Office', 'libreoffice-main.png', NULL, 1, 1, '2014-09-30 23:25:34'),
(19, NULL, 'Audio', 'audio.png', 'resources/audio/', 1, 0, '2015-05-03 18:02:39'),
(20, 19, 'Music', 'music.png', '', 1, 0, '2015-05-14 21:51:10'),
(21, 19, 'Audio Books', 'audiobook.png', '', 1, 0, '2015-05-14 21:51:33'),
(22, 2, 'Arts', 'Arts.png', NULL, 1, 1, '2015-07-26 20:58:08'),
(23, 2, 'Health', 'Health.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(24, 2, 'Language', 'Language.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(25, 2, 'Maths', 'Maths.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(26, 2, 'Preschool', 'Preschool.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(27, 2, 'ProDev', 'ProDev.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(28, 2, 'Science', 'Science.png', NULL, 1, 1, '2015-07-26 21:01:38'),
(29, 2, 'Social', 'Social.png', NULL, 1, 1, '2015-07-26 21:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_minimumage` tinyint(4) DEFAULT NULL,
  `item_maximumage` tinyint(4) DEFAULT NULL,
  `item_gender` tinyint(4) NOT NULL COMMENT 'male, female, both',
  `item_type` tinyint(4) NOT NULL COMMENT 'fun, educational, etc.',
  `item_filename` varchar(255) NOT NULL,
  `item_iconfilename` varchar(255) DEFAULT NULL,
  `item_numberaccess` int(11) DEFAULT NULL,
  `item_lastaccess` datetime DEFAULT NULL,
  `item_usedtime` int(11) DEFAULT NULL,
  `item_status` tinyint(4) NOT NULL COMMENT 'enabled or disabled',
  `item_creationuser` int(11) NOT NULL,
  `item_createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_id`),
  KEY `fk_item_category1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores items information. Items are subsets of categories, l';

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `category_id`, `item_name`, `item_minimumage`, `item_maximumage`, `item_gender`, `item_type`, `item_filename`, `item_iconfilename`, `item_numberaccess`, `item_lastaccess`, `item_usedtime`, `item_status`, `item_creationuser`, `item_createdtime`) VALUES
(1, 4, 'Madbomber', 6, 12, 0, 1, 'madbomber', 'madbomber.xpm', 4, '2015-07-08 22:18:52', 116, 1, 1, '2014-07-13 11:13:41'),
(2, 4, 'SuperTux', 0, 127, 0, 1, 'supertux', 'icon.xpm', 11, '2015-03-16 21:55:36', 1919, 1, 1, '2014-07-13 01:08:45'),
(3, 5, 'Koules', 6, 12, 0, 1, 'koules', 'koules.png', 10, '2014-10-16 00:36:42', 56, 1, 1, '2014-07-13 11:13:41'),
(4, 5, 'Funnyboat', 6, 12, 0, 1, 'funnyboat', 'funnyboat.xpm', 3, '2015-07-26 13:46:18', 360, 1, 1, '2014-07-13 11:13:41'),
(5, 6, 'KGeography', 0, 127, 0, 1, 'kgeography', 'kgeography.png', 4, '2014-07-23 21:18:10', 48, 1, 1, '2014-07-13 00:47:11'),
(6, 7, 'Tuxtype', 2, 12, 0, 2, 'tuxtype', 'tuxtype.ico', 1, '2014-10-11 11:23:34', 71, 1, 1, '2014-07-13 11:13:41'),
(7, 7, 'Parley', 0, 127, 0, 2, 'parley', 'parley.png', 2, '2014-09-17 01:13:44', 9, 1, 1, '2014-07-13 00:57:37'),
(8, 7, 'Marble', 0, 127, 0, 2, 'marble', 'marble.png', 2, '2014-10-01 03:37:38', 98, 1, 1, '2014-07-13 00:57:37'),
(9, 7, 'KWordQuiz', 0, 127, 0, 2, 'kwordquiz', 'kwordquiz.png', NULL, NULL, NULL, 1, 1, '2014-07-13 00:57:36'),
(10, 7, 'KTurtle', 0, 127, 0, 2, 'kturtle', 'kturtle.png', 1, '2014-09-14 20:42:05', 17, 1, 1, '2014-07-13 00:57:36'),
(11, 7, 'KTouch', 0, 127, 0, 2, 'ktouch', 'ktouch.png', NULL, NULL, NULL, 1, 1, '2014-07-13 00:57:35'),
(12, 7, 'KStars', 0, 127, 0, 2, 'kstars', 'kstars.png', 1, '2014-09-17 01:13:34', 50, 1, 1, '2014-07-13 00:52:33'),
(13, 7, 'KmPlot', 0, 127, 0, 2, 'kmplot', 'kmplot.png', NULL, NULL, NULL, 1, 1, '2014-07-13 00:52:32'),
(14, 12, 'LinCity-NG', 0, 127, 0, 2, 'lincity-ng', 'lincity-ng.png', 2, '2014-10-01 04:24:36', 85, 1, 1, '2014-07-13 01:01:09'),
(15, 7, 'Kig', 0, 127, 0, 2, 'kig', 'kig.png', 1, '2014-09-17 01:06:02', 0, 1, 1, '2014-07-13 00:52:31'),
(16, 7, 'Kiten', 0, 127, 0, 2, 'kiten', 'kiten.png', 20, '2014-10-19 15:14:55', 45, 1, 1, '2014-07-13 00:52:32'),
(17, 7, 'KHangMan', 0, 127, 0, 2, 'khangman', 'khangman.png', 1, '2014-09-17 01:07:41', 57, 1, 1, '2014-07-13 00:48:20'),
(18, 7, 'KBruch', 0, 127, 0, 2, 'kbruch', 'kbruch.png', 1, '2014-09-27 01:07:32', 11, 1, 1, '2014-07-13 00:45:23'),
(19, 7, 'Kalzium', 0, 127, 0, 2, 'kalzium', 'kalzium.png', 5, '2014-11-02 13:59:44', 526, 1, 1, '2014-07-13 00:42:17'),
(20, 7, 'KAlgebra', 0, 127, 0, 2, 'kalgebra', 'kalgebra.png', 6, '2015-04-05 16:03:22', 369, 1, 1, '2014-07-13 00:38:41'),
(22, 7, 'Free-Civ', 0, 127, 0, 2, 'freeciv-gtk2', 'freeciv-client.png', 7, '2015-07-26 13:45:33', 340, 1, 1, '2014-07-13 01:01:09'),
(23, 18, 'Libre Office', 0, 127, 0, 2, 'libreoffice', 'libreoffice-main.png', 6, '2014-10-22 21:21:52', 138, 0, 1, '2014-07-13 01:09:35'),
(24, 7, 'Gcompris', 2, 6, 0, 2, 'gcompris', 'gcompris.png', NULL, NULL, NULL, 1, 1, '2014-07-13 11:13:41'),
(25, 7, 'Childsplay', 2, 6, 0, 2, 'childsplay', 'childsplay.png', 3, '2015-07-26 13:45:21', 49, 1, 1, '2014-07-13 11:13:41'),
(26, 7, 'Lmemory', 2, 6, 0, 2, 'lmemory', 'lmemory.xpm', 1, '2014-09-27 01:03:19', 3, 1, 1, '2014-07-13 11:13:41'),
(27, 4, 'Supertuxkart', 6, 12, 0, 2, 'supertuxkart', 'supertuxkart.xpm', 3, '2015-02-10 23:31:16', 1027, 1, 1, '2014-07-13 11:13:41'),
(28, 7, 'Tuxmath', 2, 12, 0, 2, 'tuxmath', 'tuxmath.ico', NULL, NULL, NULL, 1, 1, '2014-07-13 11:13:41'),
(29, 8, 'Jumpnbump', 6, 12, 0, 1, 'jumpnbump', 'jumpnbump.xpm', 1, '2014-09-14 22:07:28', 62, 1, 1, '2014-07-13 11:13:41'),
(30, 11, 'Tuxpaint', 2, 12, 0, 1, 'tuxpaint', 'tuxpaint.png', 3, '2014-10-19 23:20:03', 161, 1, 1, '2014-07-13 11:13:41'),
(31, 10, 'Gtans', 6, 12, 0, 1, 'gtans', 'gtans.png', 5, '2014-10-30 10:30:42', 3185, 1, 1, '2014-07-13 11:13:41'),
(32, 10, 'Vodovod', 6, 12, 0, 1, 'vodovod', 'vodovod.png', NULL, NULL, NULL, 1, 1, '2014-07-13 11:13:41'),
(33, 10, 'Jigzo', 2, 6, 0, 1, 'jigzo', 'jigzo.xpm', 2, '2014-09-17 01:33:10', 196, 1, 1, '2014-07-13 11:13:41'),
(34, 10, 'Hex-a-hop', 6, 12, 0, 1, 'hex-a-hop', 'hex-a-hop.xpm', 5, '2014-10-01 03:31:33', 319, 1, 1, '2014-07-13 11:13:41'),
(35, 10, 'Frozen Bubble', 0, 127, 0, 1, 'frozen-bubble', 'frozen-bubble-icon-32x32.png', 4, '2015-07-26 13:46:09', 367, 1, 1, '2014-07-13 01:01:09'),
(36, 10, 'Kanagram', 0, 127, 0, 1, 'kanagram', 'kanagram.png', 2, '2015-07-08 22:26:19', 156, 1, 1, '2014-07-13 00:43:46'),
(37, 11, 'InkScape', 0, 127, 0, 1, 'inkscape', 'inkscape.png', 1, '2014-07-27 11:44:42', 36, 1, 1, '2014-07-13 01:08:46'),
(38, 11, 'Gimp', 0, 127, 0, 1, 'gimp', 'gimp.png', 1, '2014-08-24 16:01:02', 18, 1, 1, '2014-07-13 01:08:46'),
(39, 12, 'Pingus', 0, 127, 0, 1, 'pingus', 'pingus-icon.png', 7, '2014-09-07 19:50:48', 534, 1, 1, '2014-07-13 01:08:45'),
(40, 12, 'Holotz-Castle', 0, 127, 0, 1, 'holotz-castle', 'icon.bmp', 9, '2015-07-05 15:42:59', 6749, 1, 1, '2014-07-13 01:08:47'),
(41, 15, 'Fire', 6, 12, 0, 2, 'fire/start.shtm.htm', 'fire.png', 13, '2015-07-26 13:40:14', 894, 1, 1, '2014-07-15 11:13:41'),
(42, 15, 'The World Factbook', 6, 12, 0, 2, 'ciafactbook/index.html', 'ciafactbook.png', 13, '2015-07-26 13:43:05', 2093, 1, 1, '2014-07-15 11:13:41'),
(43, 15, 'Wikipedia for School', 6, 12, 0, 2, 'wikipedia_en_for_schools_2013.zim', 'wiki.png', 31, '2015-07-26 13:42:51', 2173, 1, 1, '2014-07-15 11:13:41'),
(44, 17, 'Bygosh', 6, 12, 0, 2, 'bygosh/index.html', 'bygosh.png', 9, '2015-07-26 13:37:16', 924, 1, 1, '2014-07-15 11:13:41'),
(45, 16, 'English 1', 6, 12, 0, 2, 'English_CD_V1/Index.html', 'english_1.png', 9, '2015-07-26 13:39:24', 1214, 1, 1, '2014-07-15 11:13:41'),
(46, 16, 'English 2', 6, 12, 0, 2, 'English_CD_V2/Index.html', 'english_2.png', 5, '2015-07-26 13:39:52', 712, 1, 1, '2014-07-15 11:13:41'),
(47, 16, 'English 3', 6, 12, 0, 2, 'English_CD_V3/Index.html', 'english_3.png', 1, '2015-07-26 13:40:02', 8, 1, 1, '2014-07-15 11:13:41'),
(48, 16, 'Learn English Today', 6, 12, 0, 2, 'LET/index.html', 'let.png', 3, '2015-07-26 13:40:26', 21, 1, 1, '2014-07-15 11:13:41'),
(49, 15, 'ooo4kids', 6, 12, 0, 2, 'ooo4kids', 'ooo4kids.png', 20, '2014-10-01 00:05:38', 639, 0, 1, '2014-07-15 11:13:41'),
(50, 17, 'Abroad', 0, 127, 0, 2, 'abroad.pdf', 'abroad.png', 8, '2015-07-26 13:24:48', 548, 1, 1, '2014-10-06 20:44:45'),
(51, 17, 'Adventures of Huckleberry Finn', 10, 127, 0, 2, 'adventureshuckle.pdf', 'adventureshuckle.png', 5, '2015-07-26 13:26:27', 280, 1, 1, '2014-10-06 20:44:45'),
(52, 17, 'Adventures of Tom Sawyer', 10, 127, 0, 2, 'adventuresoftoms.pdf', 'adventuresoftoms.png', 1, '2014-10-30 10:19:58', 137, 1, 1, '2014-10-06 20:44:45'),
(53, 17, 'Beauty and the Beast', 4, 99, 0, 2, 'beautythebeast.pdf', 'beautythebeast.png', 5, '2015-01-05 22:12:41', 207, 1, 1, '2014-10-06 20:44:45'),
(54, 17, 'Cinderella', 4, 99, 0, 2, 'cinderella.pdf', 'cinderella.png', 6, '2014-12-29 22:28:38', 488, 1, 1, '2014-10-06 20:44:45'),
(55, 17, 'Goody Two Shoes', 4, 99, 0, 2, 'goodytwoshoes.pdf', 'goodytwoshoes.png', 2, '2014-10-25 22:44:05', 132, 1, 1, '2014-10-06 20:44:45'),
(56, 17, 'Mother Goose''s Nursery Rhymes', 2, 6, 0, 2, 'mothergoosesnurseryrhymes.pdf', 'mothergoosesnurseryrhymes.png', 1, '2014-10-26 01:33:09', 14, 1, 1, '2014-10-06 20:44:45'),
(57, 17, 'Poems My Children Love Best of All', 2, 9, 0, 2, 'poemsmychildrenlove.pdf', 'poemsmychildrenlove.png', 1, '2014-10-26 01:35:40', 48, 1, 1, '2014-10-06 20:44:45'),
(58, 17, 'Simple Poems for Infant Minds', 2, 5, 0, 2, 'simplepoemsfor.pdf', 'simplepoemsfor.png', 1, '2014-10-26 01:32:48', 33, 1, 1, '2014-10-06 20:44:45'),
(59, 17, 'The Sleeping Beauty and Other Fairy Tales', 4, 8, 0, 2, 'sleepingbeauty.pdf', 'sleepingbeauty.png', NULL, NULL, NULL, 1, 1, '2014-10-06 20:44:45'),
(60, 17, 'The Snow Image', 0, 127, 0, 2, 'snowimagechildish.pdf', 'snowimagechildish.png', NULL, NULL, NULL, 1, 1, '2014-10-06 20:44:45'),
(61, 17, 'The Story of Doctor Dolittle', 4, 10, 0, 2, 'storyofdoctordolittle.pdf', 'storyofdoctordolittle.png', NULL, NULL, NULL, 1, 1, '2014-10-06 20:44:45'),
(62, 17, 'The Wonderful Wizard of Oz', 6, 99, 0, 2, 'wonderfulwizard.pdf', 'wonderfulwizard.png', 2, '2014-10-26 22:37:25', 60, 1, 1, '2014-10-06 20:44:45'),
(63, 18, 'LibreOffice Writer', 0, 127, 0, 2, 'libreoffice --writer', 'libreoffice-writer.png', 13, '2015-07-26 13:40:46', 0, 1, 1, '2014-10-28 00:19:11'),
(64, 18, 'LibreOffice Calc', 0, 127, 0, 2, 'libreoffice --calc', 'libreoffice-calc.png', 8, '2015-07-08 22:21:24', 0, 1, 1, '2014-10-28 00:19:11'),
(65, 18, 'LibreOffice Impress', 0, 127, 0, 2, 'libreoffice --impress', 'libreoffice-impress.png', 4, '2015-01-05 22:09:24', 0, 1, 1, '2014-10-28 00:19:11'),
(66, 18, 'LibreOffice Draw', 0, 127, 0, 2, 'libreoffice --draw', 'libreoffice-draw.png', 2, '2014-12-29 22:33:20', 0, 1, 1, '2014-10-28 00:19:11'),
(67, 18, 'LibreOffice Base', 0, 127, 0, 2, 'libreoffice --base', 'libreoffice-base.png', 7, '2015-07-26 13:40:30', 0, 1, 1, '2014-10-28 00:19:11'),
(68, 18, 'LibreOffice Math', 0, 127, 0, 2, 'libreoffice --math', 'libreoffice-math.png', 2, '2015-01-05 22:12:53', 0, 1, 1, '2014-10-28 00:19:11'),
(69, 22, 'PBS Videos - Arts', 4, 99, 0, 2, 'PBS_Arts_04-06.htm', 'Arts.png', 12, '2015-07-27 00:14:26', 4189, 1, 1, '2014-12-28 19:48:46'),
(70, 22, 'PBS Videos - Arts', 6, 99, 0, 2, 'PBS_Arts_06-08.htm', 'Arts.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(71, 22, 'PBS Videos - Arts', 8, 99, 0, 2, 'PBS_Arts_08-10.htm', 'Arts.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(72, 22, 'PBS Videos - Arts', 10, 99, 0, 2, 'PBS_Arts_10-12.htm', 'Arts.png', 3, '2015-07-27 00:14:51', 13, 1, 1, '2014-12-28 19:48:46'),
(73, 22, 'PBS Videos - Arts', 12, 99, 0, 2, 'PBS_Arts_12+.htm', 'Arts.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(74, 23, 'PBS Videos - Health', 4, 99, 0, 2, 'PBS_Health_04-06.htm', 'Health.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(75, 23, 'PBS Videos - Health', 6, 99, 0, 2, 'PBS_Health_06-08.htm', 'Health.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(76, 23, 'PBS Videos - Health', 8, 99, 0, 2, 'PBS_Health_08-10.htm', 'Health.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(77, 23, 'PBS Videos - Health', 10, 99, 0, 2, 'PBS_Health_10-12.htm', 'Health.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(78, 23, 'PBS Videos - Health', 12, 99, 0, 2, 'PBS_Health_12+.htm', 'Health.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(79, 24, 'PBS Videos - Language', 4, 99, 0, 2, 'PBS_Language_04-06.htm', 'Language.png', 3, '2015-07-27 00:17:09', 28, 1, 1, '2014-12-28 19:48:46'),
(80, 24, 'PBS Videos - Language', 6, 99, 0, 2, 'PBS_Language_06-08.htm', 'Language.png', 3, '2015-07-27 00:17:18', 8, 1, 1, '2014-12-28 19:48:46'),
(81, 24, 'PBS Videos - Language', 8, 99, 0, 2, 'PBS_Language_08-10.htm', 'Language.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(82, 24, 'PBS Videos - Language', 10, 99, 0, 2, 'PBS_Language_10-12.htm', 'Language.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(83, 24, 'PBS Videos - Language', 12, 99, 0, 2, 'PBS_Language_12+.htm', 'Language.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(84, 25, 'PBS Videos - Maths', 4, 99, 0, 2, 'PBS_Maths_04-06.htm', 'Maths.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(85, 25, 'PBS Videos - Maths', 6, 99, 0, 2, 'PBS_Maths_06-08.htm', 'Maths.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(86, 25, 'PBS Videos - Maths', 8, 99, 0, 2, 'PBS_Maths_08-10.htm', 'Maths.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(87, 25, 'PBS Videos - Maths', 10, 99, 0, 2, 'PBS_Maths_10-12.htm', 'Maths.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(88, 25, 'PBS Videos - Maths', 12, 99, 0, 2, 'PBS_Maths_12+.htm', 'Maths.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(89, 26, 'PBS Videos - Preschool', 4, 99, 0, 2, 'PBS_Preschool_04-06.htm', 'Preschool.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(90, 27, 'PBS Videos - ProDev', 4, 99, 0, 2, 'PBS_ProDev_04-06.htm', 'ProDev.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(91, 27, 'PBS Videos - ProDev', 6, 99, 0, 2, 'PBS_ProDev_06-08.htm', 'ProDev.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(92, 27, 'PBS Videos - ProDev', 8, 99, 0, 2, 'PBS_ProDev_08-10.htm', 'ProDev.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(93, 27, 'PBS Videos - ProDev', 10, 99, 0, 2, 'PBS_ProDev_10-12.htm', 'ProDev.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(94, 27, 'PBS Videos - ProDev', 12, 99, 0, 2, 'PBS_ProDev_12+.htm', 'ProDev.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(95, 28, 'PBS Videos - Science', 4, 99, 0, 2, 'PBS_Science_04-06.htm', 'Science.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(96, 28, 'PBS Videos - Science', 6, 99, 0, 2, 'PBS_Science_06-08.htm', 'Science.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(97, 28, 'PBS Videos - Science', 8, 99, 0, 2, 'PBS_Science_08-10.htm', 'Science.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(98, 28, 'PBS Videos - Science', 10, 99, 0, 2, 'PBS_Science_10-12.htm', 'Science.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(99, 28, 'PBS Videos - Science', 12, 99, 0, 2, 'PBS_Science_12+.htm', 'Science.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(100, 29, 'PBS Videos - Social', 4, 99, 0, 2, 'PBS_Social_04-06.htm', 'Social.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(101, 29, 'PBS Videos - Social', 6, 99, 0, 2, 'PBS_Social_06-08.htm', 'Social.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(102, 29, 'PBS Videos - Social', 8, 99, 0, 2, 'PBS_Social_08-10.htm', 'Social.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(103, 29, 'PBS Videos - Social', 10, 99, 0, 2, 'PBS_Social_10-12.htm', 'Social.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(104, 29, 'PBS Videos - Social', 12, 99, 0, 2, 'PBS_Social_12+.htm', 'Social.png', 2, '2014-12-28 20:48:46', 1, 1, 1, '2014-12-28 19:48:46'),
(105, 20, 'Music Test file', 12, 99, 0, 2, 'PBS_Social_12+.htm', 'Social.png', 6, '2015-07-08 22:23:35', 75, 1, 1, '2015-04-12 18:48:46'),
(106, 21, 'Audio Books', 2, 99, 0, 2, 'index.html', 'Social.png', 18, '2015-07-27 01:28:26', 986, 1, 1, '2015-04-12 18:48:46');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_Category_Category1` FOREIGN KEY (`category_parentid`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_item_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
