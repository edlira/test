#!/bin/bash

# name: games_installation_part3.sh
#       it is based on Fernando's games_installation.sh
# 	and Ferincz Janos <janos.ferincz@gmail.com> games_installation_part2 & games_installation_part3
# author: Jörg Hohn <pcs4kids@joerg-hohn.de>
# date: 10/24/2014
# description: installs games for PCs4Kids Content Manager, makes links for each of them in games folder and in icons folder
# requirements: it needs to be executed as root, because of required games installation permissions 

#list of games and documentation to be installed
GAMES="childsplay funnyboat frozen-bubble freeciv-client-gtk gcompris gimp gtans hex-a-hop holotz-castle inkscape jigzo jumpnbump kalgebra kalzium kanagram kbruch kgeography khangman kig kiten kiwix kmplot koules kstars ktouch kturtle kwordquiz libreoffice lincity-ng lmemory madbomber midori marble parley pingus supertux supertuxkart tuxmath tuxpaint tuxtype vodovod"

HOME4KIDS=/home/pcs4kids/bin
GAMEDIR=$HOME4KIDS/resources/game
DOCDIR=$HOME4KIDS/resources/doc
IMAGEDIR=$HOME4KIDS/images

echo "installing symlinks in the following directories:
Games:		$GAMEDIR
Documentation:	$DOCDIR
Images:		$IMAGEDIR
"

echo "updating repository database"
# Games installation
apt-get update
apt-get upgrade
echo "installing games and documentation"
apt-get install $GAMES

### KIWIX repo is not available for debian wheezy, we need to download the static package
### as soon as a repository is available we can do it this way

#echo "adding or updating the repository for kiwix, the offline webcontent reader"
# kiwix: adding the repository 
#echo "deb http://www.robertix.com/telechargements/binaires/ ./" > /etc/apt/sources.list.d/kiwix.list
echo "downloading kiwix and installing it"
cd /tmp/
wget http://downloads.sourceforge.net/project/kiwix/0.9_rc2/kiwix-0.9-rc2-linux-i686.tar.bz2?r=http%3A%2F%2Fwww.kiwix.org%2Fwiki%2FSoftware&ts=1414156726&use_mirror=optimate && cd /opt/ && tar xvfj /tmp/kiwix*


echo "downloading OpenOfficeOrg for Kids and installing it"
# ooo4kids: no repository available atm., downloading from sourceforge
#ARCH=`uname -r | cut -f 3 -d "-"`
cd /tmp/
wget  http://downloads.sourceforge.net/project/educooo/OOo4Kids/Linux/deb/dists/testing/main/binary-i386/ooo4kids-en-us_1.3-1_i386.deb?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Feducooo%2Ffiles%2FOOo4Kids%2FLinux%2Fdeb%2Fdists%2Ftesting%2Fmain%2Fbinary-i386%2Fooo4kids-en-us_1.3-1_i386.deb%2Fdownload&ts=1414158017&use_mirror=netcologne && dpkg -i ooo4kids-en-us_1.3-1_i386.deb && rm  ooo4kids-en-us_1.3-1_i386.deb

echo "creating symlinks in the gamedir ($GAMEDIR)"
# Links creation for games
cd $GAMEDIR
ln -s /usr/lib/libreoffice/program/soffice libreoffice
ln -s /usr/bin/midori midori
ln -s /usr/bin/kalgebra kalgebra
ln -s /usr/bin/kalzium kalzium
ln -s /usr/bin/kanagram kanagram
ln -s /usr/bin/kbruch kbruch
ln -s /usr/bin/kgeography kgeography
ln -s /usr/bin/khangman khangman
ln -s /usr/bin/kig kig
ln -s /usr/bin/kiten kiten
ln -s /usr/bin/kmplot kmplot
ln -s /usr/bin/kstars kstars
ln -s /usr/bin/ktouch ktouch
ln -s /usr/bin/kturtle kturtle
ln -s /usr/bin/kwordquiz kwordquiz
ln -s /usr/bin/marble marble
ln -s /usr/bin/parley parley
ln -s /usr/games/frozen-bubble frozen-bubble
ln -s /usr/games/lincity-ng lincity-ng
ln -s /usr/games/freeciv-gtk2 freeciv-gtk2
ln -s /usr/games/pingus pingus
ln -s /usr/games/supertux supertux
ln -s /usr/bin/gimp gimp
ln -s /usr/bin/inkscape inkscape
ln -s /usr/games/holotz-castle holotz-castle
ln -s /usr/games/childsplay childsplay
ln -s /usr/games/funnyboat funnyboat
ln -s /usr/games/gcompris gcompris
ln -s /usr/games/gtans gtans
ln -s /usr/games/hex-a-hop hex-a-hop
ln -s /usr/games/jigzo jigzo
ln -s /usr/games/jumpnbump jumpnbump
ln -s /usr/games/koules koules
ln -s /usr/games/lmemory lmemory
ln -s /usr/games/madbomber madbomber
ln -s /usr/games/supertuxkart supertuxkart
ln -s /usr/games/tuxmath tuxmath
ln -s /usr/bin/tuxpaint tuxpaint
ln -s /usr/games/tuxtype tuxtype
ln -s /usr/games/vodovod vodovod

echo "creating symlinks in the docdir ($DOCDIR)"
# Links creation for docs
cd $DOCDIR
ln -s /opt/kiwix/kiwix kiwix
ln -s /usr/lib/ooo4kids-1.3/program/soffice ooo4kids


echo "creating symlinks in the imagedir ($IMAGEDIR)"
# Links creation for images
cd $IMAGEDIR

ln -s /usr/share/icons/gnome/32x32/categories/applications-science.png applications-science.png
ln -s /usr/share/icons/gnome/32x32/categories/applications-graphics.png applications-graphics.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-main.png libreoffice-main.png
ln -s /usr/share/icons/hicolor/32x32/apps/midori.png midori.png
ln -s /usr/share/icons/hicolor/32x32/apps/kalgebra.png kalgebra.png
ln -s /usr/share/icons/hicolor/32x32/apps/kalzium.png kalzium.png
ln -s /usr/share/icons/hicolor/32x32/apps/kanagram.png kanagram.png
ln -s /usr/share/icons/hicolor/32x32/apps/kbruch.png kbruch.png
ln -s /usr/share/icons/hicolor/32x32/apps/kgeography.png kgeography.png
ln -s /usr/share/icons/hicolor/32x32/apps/khangman.png khangman.png
ln -s /usr/share/icons/hicolor/32x32/apps/kig.png kig.png
ln -s /usr/share/icons/hicolor/32x32/apps/kiten.png kiten.png
ln -s /usr/share/icons/hicolor/32x32/apps/khangman.png khangman.png
ln -s /usr/share/icons/hicolor/32x32/apps/kmplot.png kmplot.png
ln -s /usr/share/icons/hicolor/32x32/apps/kstars.png kstars.png
ln -s /usr/share/icons/hicolor/32x32/apps/ktouch.png ktouch.png
ln -s /usr/share/icons/hicolor/32x32/apps/kturtle.png kturtle.png
ln -s /usr/share/icons/hicolor/32x32/apps/kwordquiz.png kwordquiz.png
ln -s /usr/share/icons/hicolor/32x32/apps/marble.png marble.png
ln -s /usr/share/icons/hicolor/32x32/apps/parley.png parley.png
ln -s /usr/share/games/frozen-bubble/icons/frozen-bubble-icon-32x32.png frozen-bubble-icon-32x32.png
ln -s /usr/share/app-install/icons/lincity-ng.png lincity-ng.png
ln -s /usr/share/icons/hicolor/32x32/apps/freeciv-client.png freeciv-client.png
ln -s /usr/share/games/pingus/data/images/icons/pingus-icon.png pingus-icon.png
ln -s /usr/share/games/supertux/images/icon.xpm icon.xpm
ln -s /usr/share/icons/hicolor/32x32/apps/gimp.png gimp.png
ln -s /usr/share/icons/hicolor/32x32/apps/inkscape.png inkscape.png
ln -s /usr/share/games/holotz-castle/game/icon/icon.bmp icon.bmp
ln -s /usr/share/childsplay_sp/SPData/menu/default/logo_cp_32x32.png childsplay.png
ln -s /usr/share/app-install/icons/funnyboat.xpm funnyboat.xpm
ln -s /usr/share/app-install/icons/gcompris.png gcompris.png
ln -s /usr/share/icons/hicolor/32x32/apps/gtans.png gtans.png
ln -s /usr/share/app-install/icons/hex-a-hop.xpm hex-a-hop.xpm
ln -s /usr/share/app-install/icons/jigzo.xpm jigzo.xpm
ln -s /usr/share/pixmaps/jumpnbump.xpm jumpnbump.xpm
ln -s /usr/share/app-install/icons/koules.png koules.png
ln -s /usr/share/app-install/icons/lmemory.xpm lmemory.xpm
ln -s /usr/share/app-install/icons/madbomber-icon.xpm  madbomber.xpm
ln -s /usr/share/icons/hicolor/32x32/apps/supertuxkart.xpm supertuxkart.xpm
ln -s /usr/share/tuxmath/images/icons/tuxmath.ico tuxmath.ico
ln -s /usr/share/icons/hicolor/32x32/apps/tuxpaint.png tuxpaint.png
ln -s /usr/share/tuxtype/images/icons/tuxtype.ico  tuxtype.ico
ln -s /usr/share/games/vodovod/vodovod.png vodovod.png

ln -s /usr/share/icons/kiwix/32x32/kiwix.png kiwix.png
ln -s /usr/share/icons/hicolor/32x32/apps/ooo4kids13-ooo4kids.png ooo4kids.png

ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-writer.png libreoffice-writer.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-draw.png libreoffice-draw.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-impress.png libreoffice-impress.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-calc.png libreoffice-calc.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-base.png ibreoffice-base.png
ln -s /usr/share/icons/hicolor/32x32/apps/libreoffice-math.png libreoffice-math.png


